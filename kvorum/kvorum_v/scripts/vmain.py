#!/usr/bin/env python3
# coding: utf-8
"""
  vmain.py
  KVORUM MAS - VIS
  Author: Valery Karpov

  06.02.15
  18.07.2016
  Version 1.07
  LP 03.06.2018

"""
import os, sys, time, random, math

import roslib, rospy

import gdic, tshapes, rcproto
from vizenv import TGrEnv
from vizagent import TGrAgent

# Топики ROS
from msg_kvorum.msg import viz as viz_proc
from msg_kvorum.msg import pos as viz_pos

###################################################################
Title = "Kvorum_V 1.07"

Env = None
Agents = []

MSG_AINIT = None
MSG_ADRAW = None
MSG_SET_FIELD = None

###################################################################

def Afind(id):
    global Agents
    for a in Agents:
        if(a.id == id):
            return a
    #gdic.error("AFind: agent " + str(id) + " not found")
    print("AFind: agent " + str(id) + " not found")
    return None

def proc_callback(msg):
    global MSG_AINIT, MSG_ADRAW, MSG_SET_FIELD
    print("cmd:", msg.cmd, msg.data[0].id)
    if(msg.cmd==gdic.VIZ_CMD_AINIT):
        MSG_AINIT = msg
    if(msg.cmd==gdic.VIZ_CMD_ADRAW):
        MSG_ADRAW = msg
    if(msg.cmd==gdic.VIZ_CMD_SET_FIELD):
        MSG_SET_FIELD = msg
    if(msg.cmd==gdic.VIZ_CMD_SET_STATE):
        for d in msg.data:
            agent = Afind(d.id)
            if(agent==None): continue
            agent.SetState(d.state) # Состояние агента

def CreateAgents():
    global MSG_AINIT
    if(MSG_AINIT == None): return
    for d in MSG_AINIT.data:
        a = TGrAgent(d.id, (d.x, d.y, d.a), d.shape, Env, d.show_id, d.cs)
        a.traceOn = d.traceOn
        Agents.append(a)
    MSG_AINIT = None
    for a in Agents:
        a.Draw()

def DrawAgents():
    global MSG_ADRAW
    if(MSG_ADRAW == None): return
    for d in MSG_ADRAW.data:
        agent = Afind(d.id)
        if(agent==None): return
        agent.pos = (d.x, d.y, d.a)
        agent.traceOn = d.traceOn
        agent.Draw()
    MSG_ADRAW = None

def RedrawField():
    global MSG_SET_FIELD
    if(MSG_SET_FIELD == None): return
    for d in MSG_SET_FIELD.data:
        Env.SetFieldVal(d.x, d.y, d.a, d.id)
        Env.DrawCell(d.x, d.y)
    MSG_SET_FIELD = None

################################################################################
# MAIN
################################################################################

def main(envfilename, mapfilename):

    global Env, Agents

    #
    # Инициалиизация системы моделирования
    #
    print("Create forms... ")
    tshapes.CreateTForms(Title)

    print("Init environment... ")
    Env = TGrEnv(envfilename)

    print("Init map", mapfilename, "...")
    exec (open(mapfilename).read())

    #
    # Инициалиизация ROS
    #
    rospy.init_node('kvorum_v')
    NumAgents = 100
    inpqlen = NumAgents
    sub_act = rospy.Subscriber("/viz_proc_topic", viz_proc, proc_callback, queue_size=inpqlen)

    #
    # Рисование
    #"
    print("Draw Field... ")
    Env.DrawField()
    print("Done")

    #
    # Основной цикл
    #"
    print("Start main loop")

    r = rospy.Rate(50) # 50hz
    GlobalTimer = 0
    while not rospy.is_shutdown():
        CreateAgents()
        DrawAgents()
        RedrawField()
        r.sleep()

    gdic.terminate_program()

################################################################################
#
################################################################################
if __name__ == '__main__':

    if (len(sys.argv) < 3):
        print("\n"+Title, "\n\nUsage is:", sys.argv[0], "envfile mapfile")
        sys.exit(1)

    envfile = sys.argv[1]
    mapfile = sys.argv[2]

    main(envfile, mapfile)

    mainloop()
