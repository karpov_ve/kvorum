#!/usr/bin/env python3
# coding: utf-8
"""
  tmurobot.py
  Модель робота TMU
  Author: Valery Karpov

  06.02.2015
  Version 3.11
  15.07.2021
  LP 21.01.2022

"""
import sys, roslib, rospy

from env import TEnv
from agent import TAgent, TSensor

# Топики ROS
from msg_rsaction.msg import action
from msg_ans.msg import ans

import rcproto, gdic

from fsm import *

Robots = []

G_pub_cmd = None
MSG_ANS = None

# Процедуры
( PROC_SEARCH_FOOD,       # Поиск пищи
  PROC_EAT,               # Поедание пищи. Указывается уровень
  PROC_ESCAPE,            # Убегание от препятствия
  PROC_SEARCH_SHADOW,     # Поиск тени
  PROC_SLEEP,             # Сон
  PROC_WALK,              # Свободное блуждание
  PROC_MOVE_TO_OBSTACLE,  # Движение к препятствию
  PROC_NONE,
  PROC_ONE_STEP,
  PROC_REFLEX,
  # Остальные подпрограммы являются примитивами
  PROC_STOP,
  PROC_GOFWD,
  PROC_GOBACK,
  PROC_GOLEFT,   # У этой команды может быть аргумент. Он воспринимается, как поворот на заданный угол.
  PROC_GORIGHT,  # Аналогично
  PROC_STEPFWD,
  PROC_STEPBACK,
  PROC_STEPLEFT,
  PROC_STEPRIGHT,
  PROC_BEEP,
  PROC_BEEP_START,
  PROC_BEEP_STOP,
  PROC_KILL,
  PROC_SET_STATE,
  PROC_LAST
) = range(25)

ProcNames = {
              PROC_SEARCH_FOOD:      "S.FOOD",
              PROC_EAT:              "EAT",
              PROC_ESCAPE:           "ESCAPE",
              PROC_SEARCH_SHADOW:    "S.SHADOW",
              PROC_SLEEP:            "SLEEP",
              PROC_WALK:             "WALK",
              PROC_MOVE_TO_OBSTACLE: "MOVE TO OBSTACLE",
              PROC_NONE:             "None",
              PROC_ONE_STEP:         "ONE STEP",
              PROC_REFLEX:           "REFLEX" }

# Преобразуем в вектор данных, убирая лишние заголовочные байты
def msg2data(m):
    data = []
    datalen = m[rcproto.POS_LEN]
    pkglen = datalen+rcproto.POS_LEN+1

    if(pkglen>=rcproto.RC_MAX_BUFF):
        gdic.error("msg2data: packet len error: " + str(datalen))

    for i in range(0, datalen):
        data.append(m[i+rcproto.POS_DATA])
    return data

def Super_msg2data(m):
    data = []
    datalen = m[rcproto.POS_LEN]
    pkglen = datalen+rcproto.POS_LEN+1

    if(pkglen>=rcproto.RC_MAX_BUFF):
        gdic.error("msg2data: packet len error: " + str(datalen))

    for i in range(datalen//2):
        d1 = m[i*2+rcproto.POS_DATA]
        d2 = m[i*2+1+rcproto.POS_DATA]
        data.append([d1, d2])
    return data

#
# Основной класс - робот TMU
#
class TRobot():
    def __init__(self, ca, cpub):
        global G_pub_cmd
        self.agent = ca
        # Управляющий автомат, связанный с роботом
        self.curr_fsm = None # Текущий автомат
        self.pred_fsm = None # Предыдущий автомат
        self.FSMList = []    # Список автоматов

        self.pub = cpub
        G_pub_cmd = cpub

        self.cnt_act = None
        self.NCNT = None
        self.flist = None

        self.TSOPRC5 = [0]*gdic.DATALEN_RC5Server        # TSOP
        self.Dataserver = [0]*gdic.DATALEN_I2CDataServer # Данные от i2c-сервера

        self.CurrentAction = None            # Текущее движение
        self.CurrentActionArgument = None    # Аргумент текущего движения
    #
    # Обработка принятого сообщения
    #
    def AcceptMessage(self, msg):
        if(not self.agent.alive): return

        if(msg.result==rcproto.CMD_F_ANS_GET_SUPER_LOCATOR): # Суперлокатор
            rdata = Super_msg2data(msg.data)
            # Переворачиваем (так надо: углы по-другому отсчитываются)
            n = len(rdata)
            # sic Выделяем память здесь
            self.agent.SuperLocator = [(0,0)]*n
            for i in range(n):
                self.agent.SuperLocator[i] = rdata[n-i-1]
        else:
            rdata = msg2data(msg.data)
        # sic Очень странное место (вместо 0D приходит 0A). Не знаю, как с этим бороться
        if(msg.result==rcproto.CMD_ANS_GET_SENS or msg.result==0x0A or msg.result==0x0D):
            for i in range(0, len(rdata)):
                self.agent.MainSensors[i] = rdata[i]
        if(msg.result==rcproto.CMD_ANS_GET_USR_DATA): # Локатор
            # Переворачиваем (так надо: углы по-другому отсчитываются)
            n = len(rdata)
            for i in range(0, n):
                self.agent.Locator[i] = rdata[n-i-1]
        if(msg.result==rcproto.CMD_ANS_GET_ALL_REG):
            for i in range(0, len(rdata)):
                self.agent.REGISTERS[i] = rdata[i]
        if(msg.result==rcproto.CMD_ANS_GET_I2C_DATA): # Сначала идет адрес i2c-устройства
            i2caddr = rdata[0]
            # Копируем, пропуская первый элемент (там живет адрес)
            if(i2caddr == gdic.ADDR_I2CDataServer):
                csize = min(gdic.DATALEN_I2CDataServer, len(rdata))
                for i in range(1, csize):
                    self.Dataserver[i-1] = rdata[i]
            if(i2caddr == gdic.ADDR_RC5Server):
                csize = min(gdic.DATALEN_RC5Server, len(rdata))
                for i in range(1, csize):
                    self.TSOPRC5[i-1] = rdata[i]
        return

    #
    # Запрос всех сенсоров
    #   immediate: True - отправить все запросы сразу. Опасный режим, т.к. может быть переполнение очереди сообщений
    #              False - отправлять запросы по тактам
    #
    def RequestAllSensors(self, immediate=False, req_main_sensors=True, req_locator=True, req_super_locator=True, req_i2cdata=True, req_tsoprc5=True, req_registers=True):
        if(not self.agent.alive): return
        if immediate:
            if(req_main_sensors): self.RequestSensors()
            if(req_registers): self.RequestRegisters()
            if(req_locator): self.RequestUsrData()
            if(req_i2cdata): self.RequestI2CData()
            if(req_tsoprc5): self.RequestRC5Data()
            if(req_super_locator): self.RequestSuperLocator()
        else:
            if self.flist is None:
                self.flist = []
                if(req_main_sensors): self.flist.append(self.RequestSensors)
                if(req_registers): self.flist.append(self.RequestRegisters)
                if(req_locator): self.flist.append(self.RequestUsrData)
                if(req_i2cdata): self.flist.append(self.RequestI2CData)
                if(req_tsoprc5): self.flist.append(self.RequestRC5Data)
                if(req_super_locator): self.flist.append(self.RequestSuperLocator)
                self.NCNT = len(self.flist)
                self.cnt_act = 0
            f = self.flist[self.cnt_act]
            f()
            self.cnt_act+=1
            if(self.cnt_act>=self.NCNT): self.cnt_act = 0
            return

    def RequestSensors(self):
        self.Publish(rcproto.CMD_GET_SENS)

    def RequestRegisters(self):
        self.Publish(rcproto.CMD_GET_ALL_REG)

    def RequestUsrData(self):
        self.Publish(rcproto.CMD_GET_USR_DATA)

    def RequestI2CData(self):
        self.Publish(rcproto.CMD_GET_I2C_DATA, gdic.ADDR_I2CDataServer, gdic.DATALEN_I2CDataServer)

    def RequestSuperLocator(self):
        self.Publish(rcproto.CMD_F_GET_SUPER_LOCATOR)

    def RequestRC5Data(self):
        self.Publish(rcproto.CMD_GET_I2C_DATA, gdic.ADDR_RC5Server, gdic.DATALEN_RC5Server)

    #
    # Действия
    #
    def Stop(self): self.Publish(rcproto.CMD_STOP)
    def GoFwd(self): self.Publish(rcproto.CMD_FWD)
    def GoBack(self): self.Publish(rcproto.CMD_BACK)
    def GoFastLeft(self, ang=0): self.Publish(rcproto.CMD_FAST_LEFT, ang)
    def GoFastRight(self, ang=0): self.Publish(rcproto.CMD_FAST_RIGHT, ang)
    def GoLeft(self, ang=0): self.GoFastLeft(ang)
    def GoRight(self, ang=0): self.GoFastRight(ang)

    def StepFwd(self, n): return
    def StepBack(self, n): return
    def StepLeft(self, n): return
    def StepRight(self, n): return
    # Удаляется объект на слое level в координатах агента
    def ProcEat(self, level): self.Publish(rcproto.CMD_DEBUG, level)

    # Агент становится источником сигнала value на уровне level
    def SetSrc(self, level, val):
        self.agent.SetSrc(level, val)
        self.Publish(rcproto.CMD_F_SET_A_SRC, level, val)

    def SetState(self, stat):
        self.Publish(rcproto.CMD_F_SET_STATE, stat)
        self.agent.SetState(stat)

    #
    # Сообщение выходного топика
    #
    def Publish(self, act, arg1 = 0, arg2 = 0, arg3 = 0):
        msg = action()
        msg.team_id = 1
        msg.agent_id =  self.agent.id
        msg.action = act # Command
        msg.arg1 = arg1  # Argument 1
        msg.arg2 = arg2  # Argument 2
        msg.arg3 = arg3  # Argument 3
        msg.data = []
        self.pub.publish(msg)
    #
    # Выполнение действия act с аргументом arg
    #
    def Make(self, act, arg1 = 0):
        if(not self.agent.alive): return
        #sic
        self.CurrentAction = act
        self.CurrentActionArgument = arg1

        if(act==PROC_STOP): self.Stop()
        elif(act==PROC_GOFWD): self.GoFwd()
        elif(act==PROC_GOBACK): self.GoBack()
        elif(act==PROC_GOLEFT): self.GoFastLeft(arg1)
        elif(act==PROC_GORIGHT): self.GoFastRight(arg1)
        elif(act==PROC_STEPFWD): self.StepFwd(1)
        elif(act==PROC_STEPBACK): self.StepBack(1)
        elif(act==PROC_STEPLEFT): self.StepLeft(1)
        elif(act==PROC_STEPRIGHT): self.StepRight(1)
        elif(act==PROC_KILL): KillAgent(arg1)
        elif(act==PROC_EAT): self.ProcEat(arg1)
        elif(act==PROC_SET_STATE): self.SetState(arg1)
        elif(act==PROC_BEEP_START): self.Publish(rcproto.CMD_BEEP_ON)
        elif(act==PROC_BEEP_STOP): self.Publish(rcproto.CMD_BEEP_OFF)
        return

    def EvaluateSensorsInfo(self):
        return

    def ShowStatus(self, show_main_sensors=True, show_locator=True, show_super_locator=True, show_dataserver=True, show_tsoprc5=True, show_registers=True):
        if (show_main_sensors):
            print("MainSensors", len(self.agent.MainSensors), self.agent.MainSensors)
        if (show_locator):
            print("Locator", len(self.agent.Locator), self.agent.Locator)
        if (show_super_locator):
            print("SuperLocator", len(self.agent.SuperLocator), self.agent.SuperLocator)
        if (show_dataserver):
            print("Dataserver", len(self.Dataserver), self.Dataserver)
        if (show_tsoprc5):
            print("TSOPRC5", len(self.TSOPRC5), self.TSOPRC5)
        if (show_registers):
            print("REGISTERS", len(self.agent.REGISTERS), self.agent.REGISTERS)
        return

    #
    # Работа с автоматами
    #
    def FindFSM(self, name):
        for f in self.FSMList:
            if f.Name==name:
                return f
        gdic.error("FindFSM: "+name+" not found")

    def LoadFSM(self, name, arg = None):
        self.curr_fsm = self.FindFSM(name)
        self.curr_fsm.reset()
        self.curr_fsm.arg = arg
        return self.curr_fsm

################################################################################
#
# Вспомогательные функции
#
################################################################################
#
# Системные функции
#

def RFind(id):
    global Robots
    for r in Robots:
        if(r.agent.id == id):
            return r
    gdic.error("RFind: agent "+str(id) + " not found")

def rsans_callback(msg):
    global MSG_ANS
    MSG_ANS = msg
    AcceptMsg()
    return

def AcceptMsg():
    global MSG_ANS
    if(MSG_ANS==None): return
    # Адресата извлекаем из data
    addr = MSG_ANS.data[3]
    if(addr!=0):
        r = RFind(addr)
        r.AcceptMessage(MSG_ANS)
    MSG_ANS = None
    return

def PublishCmd(act, aid, arg1 = 0, arg2 = 0, arg3 = 0):
    global G_pub_cmd
    msg = action()
    msg.team_id = 1
    msg.agent_id =  int(aid)
    msg.action = int(act) # Command
    msg.arg1 = int(arg1)  # Arguments
    msg.arg2 = int(arg2)
    msg.arg3 = int(arg3)
    msg.data = []
    G_pub_cmd.publish(msg)

def KillAgent(id):
    RFind(id).agent.Delete()
    PublishCmd(rcproto.CMD_F_DELETE_AGENT, id)

# Установить координаты агента. Аргументы: id агента, x, y, angle
def SetAgentPos(id, x, y, angle):
    PublishCmd(rcproto.CMD_F_SET_APOS, id, x, y, angle)

def SetState(id, stat):
    PublishCmd(rcproto.CMD_F_SET_STATE, id, stat)
