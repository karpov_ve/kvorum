#!/usr/bin/env python3
# coding: utf-8

"""
  gdic.py
  Глобальный словарь системы
  Author: Valery Karpov

  06.02.15
  Version 3.01
  LP 09.03.2016

"""
from __future__ import print_function

import sys

def error(msg):
    print("error:", msg)
    raise Exception(msg)
    sys.exit(1)

def terminate_program(msg = "\n\nProgram terminated"):
    print(msg)
    sys.exit(1)

def pause(s):
    print(s)
    raw_input("")

################################################################################
#
#
#
################################################################################

Delta_T = 0.1

# Слои
(
LEVEL_LIGHT,   # Освещенность
LEVEL_COLOR,   # Цвет поверхности
LEVEL_IR,      # Эфир (ИК-область)
LEVEL_TEXT,    # Фиктивный слой. Текст
LEVEL_GROUND,  # Поверхность
LEVEL_ONBOARD  # Бортовой настроечный датчик (фиктивный уровень)
) = range(6)

# Словарь цветов
LevelColor = { LEVEL_GROUND: "black", LEVEL_COLOR: "green", LEVEL_LIGHT: "yellow", LEVEL_IR: "red", LEVEL_TEXT: "black"}

################################################################################
#
################################################################################
# Состояния агента
(STAT_NORM, STAT_LEADER, STAT_S0, STAT_S1, STAT_S2) = range(5)

# Словарь цветов
VocColor = { STAT_NORM: "black", STAT_LEADER: "red", STAT_S0: "white", STAT_S1: "yellow", STAT_S2: "green"}

################################################################################
# Тип датчика:
################################################################################
ST_SHARP     = 0
ST_USONIC    = 1
ST_DETECTOR  = 2
ST_CONST     = 3

# Тип возвращаемого значения
RST_SCALAR       = 0
RST_VECTOR       = 1
RST_SUPER_VECTOR = 2

################################################################################
# Расположение датчиков (id сенсоров соответствуют номерам в массиве значений)
################################################################################
# Адреса
# Сенсоры команды CMD_GET_SENS (основной контроллер)
ADDR_MvCtl = 0
ADDR_UsrData = 1
ADDR_Encoder = 2
ADDR_SuperLocator = 64 # Адрес "суперлокатора"

ADDR_I2CDataServer = 0x19
ADDR_RC5Server = 0x39


# Длина пакетов
DATALEN_I2CDataServer = 22
DATALEN_RC5Server = 16

DATALEN_SuperLocator = 360 # Длина пакета "суперлокатора"

#
#
#
REC_SHARP_FL = 6
REC_SHARP_FR = 7
REC_SHARP_SL = 1
REC_SHARP_SR = 2
REC_SHARP_C  = 3


# Сенсоры команды CMD_GET_I2C_DATA (контроллер i2cdataserver)
ADDR_CMD_GET_I2C_DATA = 1

ADDR_CMD_GET_USR_DATA = 2

REC_EXCIT   = 0 # Возбуждение
REC_INHIBIT = 1 # Торможение
REC_SPOT    = 2 # Пятно (еда)
REC_LIGHT   = 3 # Освещенность
REC_DEBUG   = 8

#
# Команды протокола pub_viz (/viz_proc_topic)
#
VIZ_CMD_AINIT     =   0 # Команда инициализации агентов. Аргументы: [(id, x, y, a, cs, shape)]
VIZ_CMD_ADRAW     =   1 # Команда рисования агентов. Аргументы: [(id, x, y, a)]
VIZ_CMD_SET_FIELD = 100 # Установить значение поля. Аргументы: x, y, a (level), id (value)
VIZ_CMD_SET_STATE =   2 # Установить состояние агента
