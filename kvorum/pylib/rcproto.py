#!/usr/bin/env python3
# coding: utf-8
"""  
  Протокол rcX2
  Author: Valery Karpov
  Version 1.03
  22.05.2016
  LP 27.09.2022
"""

HDR_BYTE = 255

# Контрольная сумма
CS_VALUE = 0xAA

POS_ADDR = 2
POS_FROM = 3
POS_CMD  = 4
POS_LEN  = 5
POS_DATA = 6

# RC_MAX_BUFF = 64
RC_MAX_BUFF = 360*2+20

#----------------------------------------------------------
#--- Команды ---
#----------------------------------------------------------
# Движение
CMD_STOP               = 0x01  # Останов
CMD_FWD                = 0x02  # Вперед
CMD_BACK               = 0x03  # Назад
CMD_LEFT               = 0x04  # Налево (одним колесом)
CMD_RIGHT              = 0x05  # Направо (одним колесом)
CMD_FAST_LEFT          = 0x06  # Налево (танковый разворот с реверсом)
CMD_FAST_RIGHT         = 0x07  # Направо (танковый разворот с реверсом

CMD_BEEP               = 0x08  # Звуковой сигнал
CMD_BEEP_ON            = 0x09  # Включить пищалку
CMD_BEEP_OFF           = 0x0A  # Выключить пищалку

CMD_ACK                = 0x0B  # Ответ: подтверждение
CMD_GET_SENS           = 0x0C  # Запрос: Получить значения всех сенсоров (АЦП)
CMD_ANS_GET_SENS       = 0x0D  # Ответ на CMD_GET_SENS
CMD_SET_ANG            = 0x0E  # Установить угол сервопривода (град)

# Работа с регистрами
CMD_SET_REG            = 0x0F  # Установить значение регистра
CMD_GET_REG            = 0x10  # Запрос: Получить значение регистра
CMD_ANS_GET_REG        = 0x11  # Ответ на CMD_GET_REG: значение регистра
CMD_GET_ALL_REG        = 0x12  # Запрос: Получить значения всех регистров
CMD_ANS_GET_ALL_REG    = 0x13  # Ответ на CMD_GET_ALL_REG: значения всех регистров

CMD_PING               = 0xFF  # Команда опроса готовности контроллера

# Движение с дополнительным аргументом
CMD_STOP2              = 0x21
CMD_FWD2               = 0x22
CMD_BACK2              = 0x23
CMD_LEFT2              = 0x24
CMD_RIGHT2             = 0x25
CMD_FAST_LEFT2         = 0x26
CMD_FAST_RIGHT2        = 0x27
CMD_SET_SPEED          = 0x28

# Прочее
CMD_DEBUG              = 0x31  # Запуск процедуры отладки
CMD_GET_USR_DATA       = 0x32  # Запрос: Получить массив пользовательских данных
CMD_I2C                = 0x33  # Команда I2C-устройству. Формат: <адрес I2C-устройства> <команда> <аргумент1> <аргумент2>
CMD_GET_I2C_DATA       = 0x34  # Запрос: Получить массив данных от I2C-устройства.
                               # Формат: <адрес I2C-устройства> <размер запрашиваемых данных>
CMD_ANS_GET_I2C_DATA   = 0x35  # Ответ на CMD_GET_I2C_DATA
CMD_ANS_GET_USR_DATA   = 0x36  # Ответ на CMD_GET_USR_DATA

CMD_USR_INT            = 0x37  # Пользовательская команда. Интерпретация зависит от шлюза между топиками модели и реального робота
                               # Формат: <кол-во аргументов> <аргумент1> <аргумент2> ...

#
# Следующие команды работают только в модели и являются фиктивными
#
# Работа с суперлокатором
CMD_F_GET_SUPER_LOCATOR     = 0x91
CMD_F_ANS_GET_SUPER_LOCATOR = 0x92

#
# Непосредственное управление на уровне модели
#
CMD_F_DELETE_AGENT        = 0x93 # Удалить агента. Аргумент: id агента
CMD_F_SET_APOS            = 0x94 # Установить координаты агента. Аргументы: id агента, arg1=x, arg2=y, arg3=angle

CMD_F_SET_FIELD           = 0x95 # Установить значение поля. Аргументы: id=value, arg1=x, arg2=y, arg3=level
CMD_F_SET_STATE           = 0x96 # Установить статус агента. Аргумент: arg1=mode
CMD_F_SET_A_SRC           = 0x97 # Установить у агента источник сигнала value на уровне level. Аргументы: arg1=level, arg2=value

############################################################

#
# Регистры
#
REG_ID          = 0   # ID робота
REG_VERSION     = 1   # Версия
REG_ANG_STEP    = 2   # Шаг угла поворота
REG_STOP_SPEED  = 3   # Скорость останова (коррекционное значение)
REG_SPEED       = 4   # Текущая скорость
REG_LOC_ENABLE  = 5   # Разрешение поворота локатора
REG_ACK         = 6   # Флаг режима подтверждения
REG_USR         = 7   # REG_USR

REG_D1          = 8   # REG_ENC_LEFT. GlobalEncoderLeftCnt, см.
REG_D2          = 9   # REG_ENC_RIGHT. GlobalEncoderRightCnt, см.
REG_ENC_LEFT    = REG_D1
REG_ENC_RIGHT   = REG_D2

REG_BUMP_DIST   = 10  # Дистанция срабатывания цифровых (контактных) бамперов
REG_USS_DIST    = 11  # Дистанция срабатывания аналоговых (УЗД) бамперов

REG_MAXU        = 12  # maxU
REG_MAXSPEED    = 13  # maxSpeed
REG_STATUS      = 14  # Регистр статуса

############################################################

MY_ADDR = 1

# Пакет
class TPckg:
    def __init__(self):
        self.rcBUFF = [1,1,1,1,1,1,1,1,1]
        self.pckg_pos = 0
        self.LAST_CMD = 0
        self.datalen = 0

    def getdatalen(self): return self.rcBUFF[POS_LEN]

    def getpkglen(self): return self.getdatalen()+POS_LEN+1

    def getcmd(self): return self.rcBUFF[POS_CMD]

    def rcFormPackageCMDNoArg(self, addr, cmd):
        self.rcBUFF = [HDR_BYTE, HDR_BYTE, addr, MY_ADDR, cmd, 0, CS_VALUE]
        self.LAST_CMD = cmd
        self.datalen = len(self.rcBUFF)
        return self.datalen

    def rcFormPackage1B(self, addr, cmd, b):
        self.rcBUFF = [HDR_BYTE, HDR_BYTE, addr, MY_ADDR, cmd, 1, b, CS_VALUE]
        self.LAST_CMD = cmd
        self.datalen = len(self.rcBUFF)
        return self.datalen

    def Data2Pckg(self, data, addr, cmd):
        self.rcBUFF = [HDR_BYTE, HDR_BYTE, addr, MY_ADDR, cmd]
        n = len(data)
        self.rcBUFF.append(n)
        for i in range( n):
            self.rcBUFF.append(data[i])
        self.rcBUFF.append(CS_VALUE)

    def SuperData2Pckg(self, data, addr, cmd):
        self.rcBUFF = [HDR_BYTE, HDR_BYTE, addr, MY_ADDR, cmd]
        n = len(data)
        self.rcBUFF.append(n*2)
        for i in range(n):
            self.rcBUFF.append(data[i][0])
            self.rcBUFF.append(data[i][1])
        self.rcBUFF.append(CS_VALUE)
