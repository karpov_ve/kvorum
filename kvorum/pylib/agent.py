#!/usr/bin/env python3
# coding: utf-8

"""
  Agent module
  Author: Valery Karpov

  06.02.15/26.02.2017
  Version 2.14
  31.08.2020
  LP 25.08.2021

"""

from __future__ import division

import sys, random, math
import gdic, geometry

################################################################################
#
# Агент
#
################################################################################
NUM_REG = 22+3 # В tmucmd текущей версии 22. Добавляем еще три фиктивных элемента: x, y, ang
NUM_SENS = 16 + NUM_REG
NUM_LOCATOR = 50

class TAgent(object):
    def __init__(self, cid, cpos, cshape, cenv, show_id = False, csize = 0):
        # csize - размер объекта: 0 - по умолчанию, -1 - по размеру сетки, иначе - по заданному значению
        # cshape - имя формы для отображения
        self.env = cenv
        self.State = gdic.STAT_NORM  # Состояние агента
        self.id = cid                # Имя (идентификатор агента)
        self.alive = True            # Флаг того, что агент жив
        self.pos = cpos              # Местопроложение (координаты и направление) (x, y, dir)
        self.shape = cshape
        self.size = csize
        self.show_id = show_id

        self.movespeed = 0           # Текущие скорости движения и разворота
        self.rotatespeed = 0

        self.RobotMoveSpeed = 1      # Крейсерские скорости движения и разворота
        self.RobotRotateSpeed = 5

        self.traceOn = False         # Флаг режима рисования трассы

        self.DrawSensors = False

        self.Sensors = []            # Сенсоры - массив датчиков (объектов)

        self.MainSensors = [0]*NUM_SENS          # Массив значений сенсоров ходового контроллера
        self.Locator = [0]*NUM_LOCATOR           # Локатор

        #self.SuperLocator = [(0, 0)]*gdic.DATALEN_SuperLocator       # Суперлокатор
        self.SuperLocator = []                   # Суперлокатор

        self.REGISTERS = [0]*NUM_REG             # Регистры
        self.SensorsVal = [0]*NUM_SENS           # Значения сенсоров

        # "След" агента. То, что он оставляет на поле
        self.Trail = dict({ gdic.LEVEL_GROUND: 0, gdic.LEVEL_COLOR: 0, gdic.LEVEL_LIGHT: 0, gdic.LEVEL_IR: 0})
        # Здесь сохраняются атрибуты точки поля
        self.SavedTrail = dict({ gdic.LEVEL_GROUND: 0, gdic.LEVEL_COLOR: 0, gdic.LEVEL_LIGHT: 0, gdic.LEVEL_IR: 0})

        # Агент оставляет след как минимум на слое gdic.LEVEL_GROUND
        self.SetSrc(gdic.LEVEL_GROUND, 1)

    #
    # Агент становится источником сигнала value на уровне level
    #
    def SetSrc(self, level, value):
        self.Trail[level] = value

    def GetSrc(self, level):
        return self.Trail[level]

    def SetState(self, st): self.State = st  # Состояние агента

    def ClearTrail(self, x, y):
        x, y = int(x), int(y)
        for v in self.SavedTrail.items():
            level, val = v
            # Восстанавливаем значения атрибутов поля
            self.env.Field[y][x][level] = val

    def SetTrail(self, x, y):
        x, y = int(x), int(y)
        for v in self.Trail.items():
            level, val = v
            # Сохраняем значения атрибутов поля
            self.SavedTrail[level] = self.env.Field[y][x][level]
            self.env.Field[y][x][level] = val

    #
    # Перемещение в точку newpos
    #
    def MoveTo(self, newpos):
        x, y, a = newpos[0], newpos[1], newpos[2] % 360
        #if(x<0 or y<0): return
        if not self.alive: return
        (oldx, oldy) = int(self.GetX()), int(self.GetY())
        (x, y) = self.env.normalizate_xy(x, y)

        SameCell = (oldx == int(x)) and (oldy == int(y))

        if(self.env.Field[int(y)][int(x)][gdic.LEVEL_GROUND] == 0 or SameCell):
            self.ClearTrail(oldx, oldy)
            self.SetTrail(x, y)
            self.pos = [x, y, a]
            return True
        else:   # только разворот
            self.pos[2] = a
            return False

    def SetAng(self, ang):
        if(not self.alive): return
        self.pos[2] = ang % 360

    def Turn2(self, ang):
        if(not self.alive): return
        self.pos[2] = (self.pos[2] + ang) % 360

    def GetX(self): return self.pos[0]
    def GetY(self): return self.pos[1]
    def GetAng(self): return self.pos[2] % 360

    def SetSidesSpeed(self, left_speed, right_speed) :
        """ Sets speed for both left and right wheel.

        NOTE: makes sense only for some types of robots, especially suitable for
        two-wheeled ones. This is a limitation imposed by the rcX protocol. See
        CMD_SET_SPEED in arduino code, etc.
        """
        # TODO: constants for YARP4, should be moved elsewhere in code
        WHEEL_DISTANCE = 48. # cm

        self.movespeed = (right_speed + left_speed) / 2
        self.rotatespeed = (right_speed - left_speed) / WHEEL_DISTANCE

    def Set2Speed(self, mspeed, rspeed):
        if(not self.alive): return
        self.movespeed = mspeed
        self.rotatespeed = rspeed

    def SetMoveSpeed(self, mspeed):
        self.movespeed = mspeed

    def SetRotSpeed(self, rspeed):
        self.rotatespeed = rspeed

    def Delete(self):
        self.traceOn = False
        self.ClearTrail(self.GetX(), self.GetY())
        self.pos = (-1+self.id, -1, 90)
        self.alive = False

    def ProcessEncoders(self, movespeed, rotatespeed, dt) :
        for sensor in self.Sensors :
            if (sensor.addr != gdic.ADDR_Encoder):
                continue
            sensor.Update(movespeed, rotatespeed, dt)

    #
    # Шаг
    #
    def Step(self):
        if(not self.alive): return
        x, y, a = self.GetX(), self.GetY(), self.GetAng()

        nx = x + self.movespeed*geometry.FastCos(a)*gdic.Delta_T
        ny = y + self.movespeed*geometry.FastSin(a)*gdic.Delta_T

        # process encoders, if any
        self.ProcessEncoders(self.movespeed, self.rotatespeed, gdic.Delta_T)

        na = a + self.rotatespeed*gdic.Delta_T
        res = self.MoveTo([nx, ny, na])

        return res

    def ReadSensors(self, saddr):
        for i in range(0, NUM_SENS): self.SensorsVal[i] = 0
        for s in self.Sensors:
            if(s.addr==saddr):
                val = s.GetSData(self)
                self.SensorsVal[s.id] = val
        return self.SensorsVal

    def GetMainSensors(self):
        self.MainSensors = self.ReadSensors(gdic.ADDR_MvCtl)

        # Энкодеры
        for sensor in self.Sensors :
            if (sensor.addr != gdic.ADDR_Encoder): continue
            self.REGISTERS[sensor.id] = sensor.GetData()

        # Добавляем содержимое регистров
        self.REGISTERS[NUM_REG-3] = int(self.GetX())
        self.REGISTERS[NUM_REG-2] = int(self.GetY())
        self.REGISTERS[NUM_REG-1] = int(self.GetAng())

        for i in range(NUM_REG):
            self.MainSensors[i + NUM_SENS - NUM_REG] = self.REGISTERS[i]
        return self.MainSensors;

    def ReadLocator(self, saddr):
        for s in self.Sensors:
            if(s.addr==saddr):
                self.Locator = s.GetSData(self)
                # sic Переворачиваем
                self.Locator.reverse()
                return self.Locator
        return self.Locator

    def ReadSuperLocator(self, saddr):
        for s in self.Sensors:
            if(s.addr==saddr):
                self.SuperLocator = s.GetSData(self)
                # sic Переворачиваем
                self.SuperLocator.reverse()                
                return self.SuperLocator
        return self.SuperLocator

    def GetLocator(self):
        self.ReadLocator(gdic.ADDR_UsrData)
        return self.Locator

    def GetSuperLocator(self):
        self.ReadSuperLocator(gdic.ADDR_SuperLocator)
        return self.SuperLocator

    def GetAllRegisters(self):
        # Добавляем тройку: x, y, ang
        self.REGISTERS[NUM_REG-3] = int(self.GetX())
        self.REGISTERS[NUM_REG-2] = int(self.GetY())
        self.REGISTERS[NUM_REG-1] = int(self.GetAng())
        return self.REGISTERS

    def GetRegister(self, n):
        if(n>=0 and n<len(self.REGISTERS)):
            return self.REGISTERS[n]
        return 255

################################################################################
#
# Сенсор
#
################################################################################
CContext = { "agent": None, "level": 0}

class TSensor(object):
    def __init__(self, caddr, cid,  cdir, cang, cR, cLevel, ctip, cvaltype = gdic.RST_SCALAR, cfproc = None, cval = 0):
        self.addr = caddr    # Адрес
        self.id = cid        # Идентификатор сенсора. Соответствует индексу в массиве сенсоров
        self.dir = cdir      # Направление относительно робота (x, y, dir)
        self.ang = cang      # Угол раствора
        self.R = cR          # Радиус действия. Если R=0, то это - точечный датчик
        self.Level = cLevel  # Рабочий слой сенсора
        self.tip = ctip      # Тип датчика: gdic.ST_SHARP, gdic.ST_USONIC, gdic.ST_DETECTOR, gdic.ST_CONST
        self.valtype = cvaltype # Тип результата
        self.fproc = cfproc  # Функция постобработки сигнала (или None)
        self.cvalue = cval   # Значение датчика для типа ST_CONST

    def GetSData(self, agent):
        # Константная величина
        if(self.tip==gdic.ST_CONST):
            return self.cvalue
        val = 0
        if(self.valtype==gdic.RST_SCALAR): val = self.GetScalar(agent)
        elif(self.valtype==gdic.RST_VECTOR): val = self.GetVector(agent, True)
        elif(self.valtype==gdic.RST_SUPER_VECTOR): val = self.GetVector(agent, False)
        elif(self.valtype==gdic.RST_ENCODER): val = self.GetEncoderData()
        else: gdic.error("TSensor.GetSData: unknown valtype")
        return val

    def nang(self, a1, a2):
        a1 = int(a1) % 360
        a2 = int(a2) % 360
        if(a1<0): a1 = 360+a1;
        if(a2<0): a2 = 360+a2;

        if(a1>a2): a1, a2 = a2, a1

        # Особое расположение
        if(a1<=90 and a2>=270): # Опять меняем
            a1, a2 = a2, a1
        return (a1, a2)

    def EvalResult(self, e, res, isScalar):
        rv, val = 0, 0
        if(e): # что-то найдено
            if(res == None): gdic.error("*** EvalResult")
            else: (dist, val) = res
            if self.tip==gdic.ST_SHARP: rv = self.R - dist
            elif self.tip==gdic.ST_USONIC: rv = dist
            elif self.tip==gdic.ST_DETECTOR: rv = val
        if not (self.fproc is None): rv = self.fproc(svalue)
        if isScalar: svalue = rv
        else: svalue = (rv, val)
        return svalue

    def GetScalar(self, agent):
        # Координаты сенсора и углы обзора
        sx, sy, sa = agent.GetX(), agent.GetY(), self.dir + agent.GetAng()
        a1, a2 = self.nang(sa-self.ang/2, sa+self.ang/2)

        global CContext
        CContext["level"] = self.Level
        CContext["agent"] = agent
        if(self.R == 0): # Точечный датчик
            # Датчик расположен со смещением
            sx = int(sx)
            sy = int(sy)
            for x in range(sx-1, sx+1+1):
                for y in range(sy-1, sy+1+1):
                    if(x!=sx and y!=sy):
                        obj = agent.env.GetObject(x, y)
                        value = obj[self.Level]
                        if(value!=0): return value
            return 0
        # Датчик секторного типа
        e, res = geometry.BresFillArc(sx, sy, a1, a2, self.R, SPlot)
        value = self.EvalResult(e, res, True)
        return value

    def GetVector(self, agent, isScalar):
        # Координаты сенсора и углы обзора
        sx, sy, sa = agent.GetX(), agent.GetY(), self.dir + agent.GetAng()
        a1, a2 = sa-self.ang/2, sa+self.ang/2

        global CContext
        CContext["level"] = self.Level
        CContext["agent"] = agent

        # Вычисляем шаг, зависящий от типа локатора
        if(isScalar):
            step = float(self.ang)/NUM_LOCATOR
            if(step<1): # Угол меньше, чем максимальное количество элементов
                avnum = int(self.ang)
                step = 1
            else:
                avnum = NUM_LOCATOR
        else:
            step = 1
            avnum = self.ang # 360

        v = []
        a = a1
        if(a1<a2): # норма
            while (a<a2 and len(v)<avnum):
                aa = int(a)
                e, res = geometry.BresLine(int(sx), int(sy), int(a), self.R, SPlot)
                value = self.EvalResult(e, res, isScalar)
                v.append(value)
                a+=step
        else: # Переход через 0
            while (a<360 and len(v)<avnum):
                e, res = geometry.BresLine(int(sx), int(sy), a, self.R, SPlot)
                a+=step
                value = self.EvalResult(e, res, isScalar)
                v.append(value)
            a = 0
            while (a<a2 and len(v)<avnum):
                e, res = geometry.BresLine(int(sx), int(sy), a, self.R, SPlot)
                a+=step
                value = self.EvalResult(e, res, isScalar)
                v.append(value)
        return v

class TEncoder(TSensor) :
    """ Класс энкодера, придерживающийся интерфейса общего датчика TSensor, однако
      добавляющего нужные параметры и автозаполняющий ненужные.

      Полагается двухколесный робот.

      TODO: proper min / max for encoder values and 2-byte representation

      sensor_id : номер
      encoder_side : для энкодеров < 0 лево, > 0 право
    """
    def __init__(self, sensor_id, encoder_side,
            wheel_radius, resolution, wheel_distance):
        super(TEncoder, self).__init__(gdic.ADDR_Encoder, sensor_id, 0, 0, 0, None, None)

        self.wheel_radius = wheel_radius
        self.resolution = resolution # ticks per turn
        self.wheel_distance = wheel_distance # distance between wheels

        self.encoder_side = encoder_side # для энкодеров < 0 лево, > 0 право

        self.ticks = 0 # ticks registered by the encoder

        self.TICKS_MAX = 255
        self.TICKS_MIN = 0

    def Update(self, fwd_speed, rot_speed, dt) :
        """
          fwd_speed : direct movement, in cm / s
          rot_speed : rotation speed in place, in degrees / s
          dt : time passed since last step, in s
        """
        if (self.encoder_side < 0):
            # left encoder
            self.ticks += ( fwd_speed * dt / (2*math.pi*self.wheel_radius) ) * self.resolution
            self.ticks += ( ( (rot_speed * dt / 360.) * math.pi
                    * self.wheel_distance ) / (2*math.pi*self.wheel_radius) ) * self.resolution
        else:
            # right encoder
            self.ticks += ( fwd_speed * dt / (2*math.pi*self.wheel_radius) ) * self.resolution
            self.ticks -= ( ( (rot_speed * dt / 360.) * math.pi
                    * self.wheel_distance ) / (2*math.pi*self.wheel_radius) ) * self.resolution

        if (self.ticks > self.TICKS_MAX): self.ticks = self.TICKS_MIN

    def GetData(self): return self.ticks


################################################################################
#
# Чтение/рисование
#
################################################################################

def SPlot(x, y):

    global CContext

    agent = CContext["agent"]
    env = agent.env
    level = CContext["level"]
    myx = int(agent.pos[0])
    myy = int(agent.pos[1])
    x = int(x)
    y = int(y)
    if(myx==x and myy==y): return (False, None)

    # Анализ точки x, y
    obj = env.GetObject(x, y)

    p = obj[level]
    if(p!=0): return (True, p)

    return (False, None)
