# coding: utf-8
import fsm_alternative as fsm_lib


class FsmObstacleAvoidance(fsm_lib.TAutomaton):
    def __init__(self):
        fsm_lib.TAutomaton.__init__(self, 'ObstacleAvoidance', ['Obst', 'Init', 'Q11', 'Q12', 'Q13', 'Q21', 'Q22', 'R', 'R1', 'R2', 'Q31', 'Q32', 'Q33', 'PreT', 'T'], ['T'], 'Init')

        self.add_rule(fsm_lib.TRule('Init', 'Obst', 'else', 'self.MAX_ALIVE = 100; self.TTurnObst = 10; self.TGoObstFwd = 10; self.TGoObstBack = 3', 3))
        self.add_rule(fsm_lib.TRule('Obst', 'Q11', 'self.isObstRight()', 'pass', 3))
        self.add_rule(fsm_lib.TRule('Obst', 'Q21', 'self.isObstFwd()', 'pass', 2))
        self.add_rule(fsm_lib.TRule('Obst', 'Q31', 'self.isObstLeft()', 'pass', 4))
        self.add_rule(fsm_lib.TRule('Obst', 'T', 'else', 'self.goStop()', 1))
        self.add_rule(fsm_lib.TRule('Obst', 'PreT', 'self.steps_counter > self.MAX_ALIVE', 'pass', 0))
        self.add_rule(fsm_lib.TRule('Q11', 'Q11', 'self.isObstRight()', 'self.turnLeft()', 4))
        self.add_rule(fsm_lib.TRule('Q11', 'Q12', 'else', 'self.T = 0', 1))
        self.add_rule(fsm_lib.TRule('Q11', 'Obst', 'self.isObstLeft()', 'pass', 4))
        self.add_rule(fsm_lib.TRule('Q11', 'PreT', 'self.steps_counter > self.MAX_ALIVE', 'pass', 0))
        self.add_rule(fsm_lib.TRule('Q12', 'Q12', 'self.T < self.TGoObstFwd', 'self.goFwd(); self.T += 1;', 3))
        self.add_rule(fsm_lib.TRule('Q12', 'Q13', 'else', 'self.T = 0', 1))
        self.add_rule(fsm_lib.TRule('Q12', 'Obst', 'self.isObstLeft() or self.isObstRight() or self.isObstFwd()', 'pass', 2))
        self.add_rule(fsm_lib.TRule('Q12', 'PreT', 'self.steps_counter > self.MAX_ALIVE', 'pass', 0))
        self.add_rule(fsm_lib.TRule('Q13', 'Q13', 'self.T < self.TTurnObst', 'self.turnRight(); self.T += 1;', 3))
        self.add_rule(fsm_lib.TRule('Q13', 'Obst', 'else', 'pass', 2))
        self.add_rule(fsm_lib.TRule('Q13', 'PreT', 'self.steps_counter > self.MAX_ALIVE', 'pass', 0))
        self.add_rule(fsm_lib.TRule('Q21', 'Q21', 'self.isObstFwd() and not self.isObstCloseBehind()', 'self.goBack()', 3))
        self.add_rule(fsm_lib.TRule('Q21', 'Q22', 'else', 'self.T = 0;', 1))
        self.add_rule(fsm_lib.TRule('Q21', 'PreT', 'self.steps_counter > self.MAX_ALIVE', 'pass', 0))
        self.add_rule(fsm_lib.TRule('Q22', 'Q22', '(self.T < self.TGoObstBack) and not self.isObstCloseBehind()', 'self.goBack(); self.T += 1;', 3))
        self.add_rule(fsm_lib.TRule('Q22', 'R', 'self.T >= self.TGoObstBack', 'self.T = 0;', 1))
        self.add_rule(fsm_lib.TRule('Q22', 'Q22', 'else', 'self.goStop(); self.T += 1', 1))
        self.add_rule(fsm_lib.TRule('Q22', 'PreT', 'self.steps_counter > self.MAX_ALIVE', 'pass', 0))
        self.add_rule(fsm_lib.TRule('R', 'R1', 'self.rand(2) == 0', 'pass', 3))
        self.add_rule(fsm_lib.TRule('R', 'R2', 'self.rand(2) == 1', 'pass', 1))
        self.add_rule(fsm_lib.TRule('R', 'R', 'else', 'pass', 1))
        self.add_rule(fsm_lib.TRule('R', 'PreT', 'self.steps_counter > self.MAX_ALIVE', 'pass', 0))
        self.add_rule(fsm_lib.TRule('R1', 'R1', 'self.T < self.TTurnObst', 'self.turnLeft(); self.T += 1', 3))
        self.add_rule(fsm_lib.TRule('R1', 'Obst', 'else', 'self.goFwd()', 1))
        self.add_rule(fsm_lib.TRule('R1', 'PreT', 'self.steps_counter > self.MAX_ALIVE', 'pass', 0))
        self.add_rule(fsm_lib.TRule('R2', 'R2', 'self.T < self.TTurnObst', 'self.turnRight(); self.T += 1', 3))
        self.add_rule(fsm_lib.TRule('R2', 'Obst', 'else', 'self.goFwd()', 1))
        self.add_rule(fsm_lib.TRule('R2', 'PreT', 'self.steps_counter > self.MAX_ALIVE', 'pass', 0))
        self.add_rule(fsm_lib.TRule('Q31', 'Q31', 'self.isObstLeft()', 'self.turnRight()', 4))
        self.add_rule(fsm_lib.TRule('Q31', 'Q32', 'else', 'self.T = 0', 1))
        self.add_rule(fsm_lib.TRule('Q31', 'Obst', 'self.isObstRight()', 'pass', 4))
        self.add_rule(fsm_lib.TRule('Q31', 'PreT', 'self.steps_counter > self.MAX_ALIVE', 'pass', 0))
        self.add_rule(fsm_lib.TRule('Q32', 'Q32', 'self.T < self.TGoObstFwd', 'self.goFwd(); self.T += 1;', 3))
        self.add_rule(fsm_lib.TRule('Q32', 'Q33', 'else', 'self.T = 0', 1))
        self.add_rule(fsm_lib.TRule('Q32', 'Obst', 'self.isObstLeft() or self.isObstRight() or self.isObstFwd()', 'pass', 2))
        self.add_rule(fsm_lib.TRule('Q32', 'PreT', 'self.steps_counter > self.MAX_ALIVE', 'pass', 0))
        self.add_rule(fsm_lib.TRule('Q33', 'Q33', 'self.T < self.TTurnObst', 'self.turnLeft(); self.T += 1;', 3))
        self.add_rule(fsm_lib.TRule('Q33', 'Obst', 'else', 'pass', 2))
        self.add_rule(fsm_lib.TRule('Q33', 'PreT', 'self.steps_counter > self.MAX_ALIVE', 'pass', 0))
        self.add_rule(fsm_lib.TRule('PreT', 'T', 'True', 'self.goStop()', 1))
