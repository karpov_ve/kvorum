#
# Параметры поля
# Этот файл читается при создании объекта Env
#

self.MAX_SCR_X = 500
self.MAX_SCR_Y = 250

self.DIM_X = 100
self.DIM_Y = 50

self.UseTorusRegime  = False # Флаг режима топологии тора
