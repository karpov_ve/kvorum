cmake_minimum_required(VERSION 2.8.3)
project(ant)

find_package(catkin REQUIRED COMPONENTS)

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES mpp_test
#  CATKIN_DEPENDS other_catkin_pkg
#  DEPENDS system_lib
)

