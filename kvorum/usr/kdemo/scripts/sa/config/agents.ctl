#
# flock hunting demo
# Описание робота
#

########################################################################
# Вспомогательная функция для инверсии датчиков
########################################################################
def f_inv(r):
    if(r==0): return 1
    else: return 0

THUNTER = "turtle"
TVICTIM = "hare"

########################################################################
# Создание робота
########################################################################

def CreateRobot(id, pos, tip, irvalue):
    global THUNTER, TVICTIM

    # Аргументы: cid, cpos, cshape, cenv, csize = 0
    A = TAgent(id, pos, tip, Env, csize = 0.4) # 0.75

    ##################################################################
    #
    # Сенсоры
    #
    # Аргументы: caddr, cid,  cdir, cang, cR, cLevel, ctip, cvaltype = gdic.ST_SCALAR, cfproc = None, cvalue = 0
    #
    ##################################################################

    #
    # *** Датчики препятствий
    #
    # USONIC левый передний
    USONIC_DIST = 10
    A.Sensors.append(TSensor(gdic.ADDR_MvCtl, 0, 20,  1, USONIC_DIST, gdic.LEVEL_GROUND, gdic.ST_USONIC))

    # USONIC правый передний
    A.Sensors.append(TSensor(gdic.ADDR_MvCtl, 1, -20, 1, USONIC_DIST, gdic.LEVEL_GROUND, gdic.ST_USONIC))

    # Суперлокатор. Он должен быть один
    # Значения помещаются в TRobot.SuperLocator
    SL_TSOP_DIST = 20
    A.Sensors.append(TSensor(gdic.ADDR_SuperLocator, 0, 0, 360, SL_TSOP_DIST, gdic.LEVEL_IR, gdic.ST_USONIC, gdic.RST_SUPER_VECTOR))

    # Агент становится источником сигнала
    # Это нужно для определения типа агента
    A.SetSrc(gdic.LEVEL_IR, irvalue)

    #
    # *** Датчики цвета поверхности (еда) и света
    #
    # Сенсор пятна (еда). Точечный датчик (радиус=0)
    A.Sensors.append(TSensor(gdic.ADDR_I2CDataServer, 1, 0, 0, 0, gdic.LEVEL_COLOR, gdic.ST_DETECTOR))

    # Сенсор света. Точечный датчик (радиус=0)
    A.Sensors.append(TSensor(gdic.ADDR_I2CDataServer, 2, 0, 0, 0, gdic.LEVEL_LIGHT, gdic.ST_DETECTOR))

    # Охотник
    if(tip==THUNTER):
        # Датчики освещенности для охотника. Нужен для обнаружения жертвы в зоне досягаемости и поедания ее
        V_DIST = 5
        A.Sensors.append(TSensor(gdic.ADDR_I2CDataServer, 0, 0, 180, V_DIST, gdic.LEVEL_LIGHT, gdic.ST_DETECTOR, gdic.RST_SCALAR))

        # Крейсерские скорости движения и разворота
        A.RobotMoveSpeed = 1
        A.RobotRotateSpeed = 5
    else:
        # Жертва является источником. Нужно для того, чтобы хищник мог обнаружить ее в непосредственной близости и
        # съесть ее
        A.SetSrc(gdic.LEVEL_LIGHT, id)

        # Крейсерские скорости движения и разворота
        A.RobotMoveSpeed = 1
        A.RobotRotateSpeed = 5


    return A

########################################################################
# Роботы
########################################################################
NUM_HUNT = 0
HUNTER_IR_CODE = 0xA0

NUM_VICTIM = 25
VICTIM_IR_CODE = 0x01

#
# Агенты являются источниками ИК-излучения, код которого не постоянный, а зависит от id агента
#

# Охотники
x0 = 1
for i in range(0, NUM_HUNT):
    Agents.append(CreateRobot(i+1, [x0+i*2, 1, 90], THUNTER, HUNTER_IR_CODE+i))

# Жертвы
x0 = 50
for i in range(0, NUM_VICTIM):
    Agents.append(CreateRobot(i+1+NUM_HUNT, [x0+i*2, 80, -90], TVICTIM, VICTIM_IR_CODE+i))

