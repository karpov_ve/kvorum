#
# flock hunting war_demo
# Описание робота
#

TIP_T1 = "p_yellow" # "turtle"
TIP_T2 = "p_red"    # "hare"

NUM_T1 = 20
NUM_T2 = 20

IR_CODE_BASE_T1 =  1
IR_CODE_BASE_T2 = NUM_T1 + 1 + 100

SL_TSOP_FAR_DIST  = 30
SL_TSOP_NEAR_DIST = 10

########################################################################
# Создание робота
########################################################################

def CreateRobot(id, pos, tip, irvalue):
    global TIP_T1, TIP_T2
    global NUM_T1, NUM_T2, IR_CODE_T1, IR_CODE_T2
    global SL_TSOP_FAR_DIST, SL_TSOP_NEAR_DIST

    # Аргументы: cid, cpos, cshape, cenv, csize = 0
    A = TAgent(id, pos, tip, Env, csize = 0.55) # 0.75

    ##################################################################
    #
    # Сенсоры
    #
    # Аргументы: caddr, cid,  cdir, cang, cR, cLevel, ctip, cvaltype = gdic.ST_SCALAR, cfproc = None, cvalue = 0
    #
    ##################################################################

    #
    # *** Датчики препятствий
    #
    USONIC_DIST = 10
    # USONIC левый передний
    A.Sensors.append(TSensor(gdic.ADDR_MvCtl, 0, 20,  1, USONIC_DIST, gdic.LEVEL_GROUND, gdic.ST_USONIC))

    # USONIC правый передний
    A.Sensors.append(TSensor(gdic.ADDR_MvCtl, 1, -20, 1, USONIC_DIST, gdic.LEVEL_GROUND, gdic.ST_USONIC))

    # Суперлокатор. Он должен быть один
    A.Sensors.append(TSensor(gdic.ADDR_SuperLocator, 0, 0, 360, SL_TSOP_FAR_DIST, gdic.LEVEL_IR, gdic.ST_USONIC, gdic.RST_SUPER_VECTOR))

    # Агент является источником IR-сигнала (gdic.LEVEL_IR)
    A.SetSrc(gdic.LEVEL_IR, irvalue)

    # Крейсерские скорости движения и разворота
    A.RobotMoveSpeed = 2
    A.RobotRotateSpeed = 5

    return A

########################################################################
# Роботы
########################################################################

#
# Агенты являются источниками ИК-излучения, код которого не постоянный, а зависит от id агента
#
id = 1
# Агенты 1 группы
x0, y0 = 2, 5
for i in range(0, NUM_T1):
    ircode = IR_CODE_BASE_T1 + i
    Agents.append(CreateRobot(id, [x0+i, y0+i, random.randint(45, 60)], TIP_T1, ircode))
    print id, ircode
    id += 1

# Агенты 2 группы
x0, y0 = 36, 65
for i in range(0, NUM_T2):
    ircode = IR_CODE_BASE_T2 + i
    Agents.append(CreateRobot(id, [x0+i, y0+i, random.randint(-110, -100)], TIP_T2, ircode))
    print id, ircode
    id += 1
