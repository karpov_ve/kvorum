#!/usr/bin/env python
# coding: utf-8
"""
  flock hunting war_demo
  477, 360
  06.02.15
  Version 1.05
  LP 26.02.2017
"""
import os, sys, time
import roslib, rospy, time, random

sys.path.append("../../pylib")
sys.path.append("src/kvorum/pylib")

import gdic, rcproto, tmurobot as tmu
from env import TEnv
from agent import TAgent, TSensor

###################################################################

Title = "War Demo 1.05"

GlobalTimer = 0
Env = None
Agents = []

# Инициалиизация системы
def InitSystem(nodename, envfilename, mapfilename, agentfilename):
    global Agents

    # Инициалиизация ROS
    rospy.init_node(nodename)

    # Входной топик
    # Подписываемся на топик ardans, второй параметр - объем кэша отправки
    inpqlen = len(Agents)*10 #7
    rospy.Subscriber("/ardans_topic", tmu.ans, tmu.rsans_callback, queue_size=inpqlen)

    # Выходной топик
    outqlen = len(Agents)*10 #7
    pub_cmd = rospy.Publisher("/actions_topic", tmu.action, queue_size=outqlen)

    # Создаем роботов
    for a in Agents:
        r = tmu.TRobot(a,pub_cmd)
        tmu.Robots.append(r)

def FindFirst(R, code, delta, distlim):
    L = len(R.agent.SuperLocator)
    for n in range(0, L):
        dist, val = R.agent.SuperLocator[n]
        if(dist<=distlim or distlim<=0) and (val != 0):
            if((val>=code) and (val<code+delta)):
                return val
    return 0

def findat(R, a1, a2, code, delta, distlim):
    num = 0
    L = len(R.agent.SuperLocator)
    if(a1>=L or a2>=L): return 0
    for n in range(a1, a2):
        dist, val = R.agent.SuperLocator[n]
        if(dist<=distlim or distlim<=0) and (val != 0):
            if((val>=code) and (val<code+delta)):
                num += 1
    return num

def Afind(id):
    global Agents
    for a in Agents:
        if(a.id == id):
            return a
    #gdic.error("AFind: agent " + str(id) + " not found")
    print "AFind: agent " + str(id) + " not found"
    return None
#
# q - номер квадранта:
#    0 - впереди
#    1 - сзади
#    2 - слева
#    3 - справа
#  углы a1, a2 - в диапазоне 0..360
#  [0]   180
#  [45]  135
#  [90]:  90
# [135]:  45
# [225]: -45
# [315]:-135
# [359]:-180
def FindIR(a, q, code, delta, distlim):
    val = 0
    if(q==0):
        a1, a2 = 135, 225
    if(q==1):
        a1, a2 = 0, 45
        a3, a4 = 315, 359
    if(q==2):
        a1, a2 = 45, 135
    if(q==3):
        a1, a2 = 225, 315
    val = findat(a, a1, a2, code, delta, distlim)
    if(val!=0): return val
    if(q==1):
        val = findat(a, a3, a4, code, delta, distlim)
    return val

########################################################################
# Вспомогательные функции для обращения к датчикам
########################################################################

def sGetFL(A):
    s = A.agent.MainSensors[0]
    if(s==0): s = 100
    return s

def sGetFR(A):
    s = A.agent.MainSensors[1]
    if(s==0): s = 100
    return s

########################################################################
#
########################################################################
def Analyze(A, friendcode, enemycode, num_frend, num_enemy, distlim):
    delta_frend = num_frend;
    delta_enemy = num_enemy;

    sit = {'fwd':[0,0], 'back':[0,0], 'left':[0,0], 'right':[0,0]}  

    sit['fwd'][0] =   FindIR(A, 0, friendcode, delta_frend, distlim)
    sit['back'][0] =  FindIR(A, 1, friendcode, delta_frend, distlim)
    sit['left'][0] =  FindIR(A, 2, friendcode, delta_frend, distlim)
    sit['right'][0] = FindIR(A, 3, friendcode, delta_frend, distlim)

    sit['fwd'][1] =   FindIR(A, 0, enemycode, delta_enemy, distlim)
    sit['back'][1] =  FindIR(A, 1, enemycode, delta_enemy, distlim)
    sit['left'][1] =  FindIR(A, 2, enemycode, delta_enemy, distlim)
    sit['right'][1] = FindIR(A, 3, enemycode, delta_enemy, distlim)

    nfriend = sit['fwd'][0]+sit['back'][0]+sit['left'][0]+sit['right'][0]
    nenemy  = sit['fwd'][1]+sit['back'][1]+sit['left'][1]+sit['right'][1]

    return sit, nfriend, nenemy

########################################################################
#
########################################################################

def MakeReflex(a): 
    rdist = 5
    act, arg = tmu.PROC_GOFWD, 0
    refl = True
    if(sGetFL(a)<rdist and sGetFR(a)<rdist):
        act, arg = tmu.PROC_GOLEFT, 5 + random.randint(-1, 1)
    elif(sGetFL(a)<rdist):
        act, arg = tmu.PROC_GORIGHT,5 + random.randint(-1, 1)
    elif(sGetFR(a)<rdist):
        act, arg = tmu.PROC_GOLEFT, 5 + random.randint(-1, 1)
    else:
        refl = False
    return refl, act, arg  

################################################################################
# MAIN
################################################################################

def main(envfilename, mapfilename, agentfilename):

    global Env, Agents, GlobalTimer

    global TIP_T1, TIP_T2, IR_CODE_T1, IR_CODE_T2, NUM_T1, NUM_T2
    global SL_TSOP_FAR_DIST, SL_TSOP_NEAR_DIST

    # Инициалиизация системы
    print "Init environment... "
    Env = TEnv(envfilename)

    print "Init map", mapfilename, "..."
    exec (open(mapfilename).read())

    print "Init agents", agentfilename, "..."
    exec (open(agentfilename).read())

    InitSystem('war_demo', envfilename, mapfilename, agentfilename)

    print "Start main loop"
    r = rospy.Rate(10) # 10 50hz

    #####################################################
    # Основной цикл
    #####################################################
    counter = 0
    while not rospy.is_shutdown():
        counter += 1

        for a in tmu.Robots:
            if(a.agent.alive==False): continue
            my_id = a.agent.id
            a.RequestAllSensors(immediate=False, req_main_sensors=True, req_locator=False, req_super_locator=True, req_i2cdata=False, req_tsoprc5=False, req_registers=False)

            a.ShowStatus(show_main_sensors=False, show_locator=False, show_super_locator=False, show_dataserver=False, show_tsoprc5=False, show_registers=False)

            act, arg = tmu.PROC_GOFWD, 0
            refl, ract, rarg = MakeReflex(a)

            if(a.agent.shape==TIP_T1):
                num_agent_frend = NUM_T1     # Количество друзей
                num_agent_enemy = NUM_T2     # Количество врагов
                friendcode = IR_CODE_BASE_T1
                enemycode = IR_CODE_BASE_T2
            else:
                num_agent_frend = NUM_T2
                num_agent_enemy = NUM_T1
                friendcode = IR_CODE_BASE_T2
                enemycode = IR_CODE_BASE_T1

            # Убираем дубли из суперлокатора
            L = len(a.agent.SuperLocator)
            for n in range(0, L):
                dist, val = a.agent.SuperLocator[n]
                if(val!=0):
                    for i in range(n+1, L):
                        dist2, val2 = a.agent.SuperLocator[i]
                        if(val==val2):
                            a.agent.SuperLocator[i] = (0, 0)
            # Анализ ситуации
            sit, nfriend, nenemy = Analyze(a, friendcode, enemycode, num_agent_frend, num_agent_enemy, SL_TSOP_FAR_DIST)

            angle = 10
            if(nenemy>0): # Бежим на врага
                if sit['fwd'][1]>0: # Враг впереди
                    act, arg = tmu.PROC_GOFWD, 0
                elif sit['left'][1]>0: # Враг  слева
                    act, arg = tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
                elif sit['right'][1]>0: # Враг  справа
                    act, arg = tmu.PROC_GORIGHT, angle + random.randint(-1, 1)
                elif sit['back'][1]>0: # Враг  сзади
                    act, arg = tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
            elif(nfriend>0): # Сближаемся со своими
                    if sit['fwd'][0]>0: 
                        act, arg = tmu.PROC_GOFWD, 0
                    elif sit['left'][0]>0:  
                        act, arg = tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
                    elif sit['right'][0]>0:
                        act, arg = tmu.PROC_GORIGHT, angle + random.randint(-1, 1)
                    elif sit['back'][0]>0:  
                        act, arg = tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
            else: # Никого нет
                act, arg = tmu.PROC_GOFWD, 0

            # Анализ ближайшего окружения
            sit2, nfriend2, nenemy2 = Analyze(a, friendcode, enemycode, num_agent_frend, num_agent_enemy, SL_TSOP_NEAR_DIST)
            if(nenemy2>0):
                # Пробуем напасть
                irc = FindFirst(a, enemycode, num_agent_enemy, SL_TSOP_NEAR_DIST)
                if(a.agent.shape==TIP_T1):
                    enemy_id = irc - IR_CODE_BASE_T2  + 1 + NUM_T1
                else:
                    enemy_id = irc - IR_CODE_BASE_T1  + 1

                print my_id, ": enemy_id =", enemy_id, "irc =", irc, "enemycode =", enemycode

                # Определяем, кто сильнее
                aen = Afind(enemy_id)
                if not aen.alive: continue
                enemy_weight = aen.id
                my_weight = a.agent.id

                rn = random.randint(1,100)
                mw = int(100.0*(nfriend2 + my_weight) / (nfriend2 + nenemy2 + my_weight + enemy_weight))
                my_win = (rn<mw)
                if my_win:
                    victim_id = enemy_id
                else:
                    victim_id = my_id

                print my_id, "?", enemy_id, "=>", victim_id
                tmu.KillAgent(victim_id)

            #################################################################
            # Рефлекс, возможные вненшие факторы
            if refl: act, arg = ract, rarg
            a.Make(act, arg)

        GlobalTimer += 1
        cnt = n1 = n2 = 0
        for a in tmu.Robots:
            if(not a.agent.alive):
                continue
            if(a.agent.id<=NUM_T1):
                n1 += 1
            else:
                n2 += 1
            cnt += 1
        print GlobalTimer, ":::", cnt, n1, n2

        r.sleep()

    gdic.terminate_program()

################################################################################
#
################################################################################
if __name__ == '__main__':

    if (len(sys.argv) < 4):
        print "\n", Title, "\n\nUsage is:", sys.argv[0], "envfile mapfile agentfile"
        sys.exit(1)

    envfile = sys.argv[1]
    mapfile = sys.argv[2]
    agentfile = sys.argv[3]

    main(envfile, mapfile, agentfile)
