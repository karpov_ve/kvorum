#!/usr/bin/env python
# coding: utf-8
"""
  flock moving
  Простое стайное движение с локальным или глобальным лидером

  06.02.15
  Version 1.04
  LP 17.05.2015
"""
import os, sys, time
import roslib, rospy, time, random

LIB_PATH = "src/kvorum/pylib"

sys.path.append("../../pylib")
sys.path.append(LIB_PATH)

import gdic, rcproto, tmurobot as tmu
from env import TEnv
from agent import TAgent, TSensor

###################################################################

Title = "Demo 1.04"

GlobalTimer = 0
Env = None
Agents = []

# Инициалиизация системы
def InitSystem(nodename, envfilename, mapfilename, agentfilename):
    global Agents

    # Инициалиизация ROS
    rospy.init_node(nodename)

    # Входной топик
    # Подписываемся на топик ardans, второй параметр - объем кэша отправки
    inpqlen = len(Agents)*7
    rospy.Subscriber("/ardans_topic", tmu.ans, tmu.rsans_callback, queue_size=inpqlen)

    # Выходной топик
    outqlen = len(Agents)*7
    pub_cmd = rospy.Publisher("/actions_topic", tmu.action, queue_size=outqlen)

    # Создаем роботов
    for a in Agents:
        r = tmu.TRobot(a,pub_cmd)
        tmu.Robots.append(r)

########################################################################
#
########################################################################

def Analyze(A, code):
    sit = {'fwd':0, 'back':0, 'left':0, 'right':0}
    f0 = 0
    f1 = 0
    f2 = 0
    f3 = 0
    if(code>0):
        if (A.TSOPRC5[0] == code): f0 = code
        if (A.TSOPRC5[1] == code): f1 = code
        if (A.TSOPRC5[2] == code): f2 = code
        if (A.TSOPRC5[3] == code): f3 = code
    else:
        f0 = A.TSOPRC5[0]
        f1 = A.TSOPRC5[1]
        f2 = A.TSOPRC5[2]
        f3 = A.TSOPRC5[3]

    maxcode = max(f0, f1, f2, f3)
    if maxcode==f0: maxdir = 'fwd'
    if maxcode==f1: maxdir = 'back'
    if maxcode==f2: maxdir = 'left'
    if maxcode==f3: maxdir = 'right'

    sit['fwd'] = f0
    sit['back'] = f1
    sit['left'] = f2
    sit['right'] = f3

    return sit, maxcode, maxdir

########################################################################
# Вспомогательные функции для обращения к датчикам
########################################################################

def sGetFL(A):
    s = A.agent.MainSensors[0]
    if(s==0): s = 100
    return s

def sGetFR(A):
    s = A.agent.MainSensors[1]
    if(s==0): s = 100
    return s

def GetLightSensor(A): return A.Dataserver[0]

def ShowStatus(A, sit):
    print A.agent.id, sit

########################################################################
#
########################################################################

def MakeReflex(a): 
    rdist = 5
    act, arg = tmu.PROC_GOFWD, 0
    refl = True
    if(sGetFL(a)<rdist and sGetFR(a)<rdist):
        #act, arg = tmu.PROC_GOLEFT, 5 + random.randint(-1, 1)
        act, arg = tmu.PROC_GOBACK, 0
    elif(sGetFL(a)<rdist):
        act, arg = tmu.PROC_GORIGHT,5 + random.randint(-1, 1)
    elif(sGetFR(a)<rdist):
        act, arg = tmu.PROC_GOLEFT, 5 + random.randint(-1, 1)
    else:
        refl = False
    return refl, act, arg

################################################################################
# MAIN
################################################################################

def main(envfilename, mapfilename, agentfilename):

    global Env, Agents, GlobalTimer

    # Инициалиизация системы
    print "Init environment... "
    Env = TEnv(envfilename)

    print "Init map", mapfilename, "..."
    exec (open(mapfilename).read())

    print "Init agents", agentfilename, "..."
    exec (open(agentfilename).read())

    InitSystem('demo', envfilename, mapfilename, agentfilename)

    print "Start main loop"
    r = rospy.Rate(10) # 50hz

    #####################################################
    # Основной цикл
    #####################################################

    while not rospy.is_shutdown():
        for a in tmu.Robots:
            a.RequestAllSensors(False)

            act, arg = tmu.PROC_GOFWD, 0

            refl, ract, rarg = MakeReflex(a)
            #################################################################
            # Рабочий
            #################################################################
            if(a.agent.shape==TIP_WORKER):
                angle = 10
                sit, maxcode, maxdir = Analyze(a, LEADER_IR_CODE) # 0
                sit, maxcode, maxdir = Analyze(a, 0) # 0
                #ShowStatus(a, sit)
                mycode =  a.agent.GetSrc(gdic.LEVEL_IR)

                if mycode<maxcode:
                #
                # Правило 1. Я слабее. Сближаемся с тем кто сильнее
                #
                    if maxdir == 'fwd':
                        act, arg = tmu.PROC_GOFWD, 0
                    elif maxdir == 'left':
                        act, arg = tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
                    elif maxdir == 'right':
                        act, arg = tmu.PROC_GORIGHT, angle + random.randint(-1, 1)
                    elif maxdir == 'back':
                        act, arg = tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
                    a.Make(tmu.PROC_SET_STATE, gdic.STAT_NORM)
                else: # Я сильнее или ничего не видно
                    behavnum = 1
                    if (behavnum == 1):
                        # Тактика №1: ничего не делаю, стою на месте, только кручусь
                        act, arg = tmu.PROC_STOP, 0
                        n = random.randint(0, 1)
                        if (n==0):
                            act, arg = tmu.PROC_GOLEFT, angle*random.randint(-1, 1)
                        if (n==1):
                            act, arg = tmu.PROC_STOP, 0
                    else:
                        # Тактика №2: куда-то иду
                        act, arg = tmu.PROC_GOFWD, 0
                        n = random.randint(0, 2)
                        if (n==0):
                            act, arg = tmu.PROC_GOLEFT, angle*random.randint(-1, 1)
                        if (n==1):
                            act, arg = tmu.PROC_GOFWD, 0
                        if (n==2):
                            act, arg = tmu.PROC_STOP, 0

                    a.Make(tmu.PROC_SET_STATE, gdic.STAT_LEADER)

                #
                # Правило 2. Стараемся держать дистанцию
                #
                normdist = 5
                if(sGetFL(a)<normdist or sGetFR(a)<normdist) and (act==tmu.PROC_GOFWD):
                    act, arg = tmu.PROC_STOP, 0

                #
                # Правило 3. Не сталкиваться. Рефлекс
                #
                if refl: act, arg = ract, rarg
            else:
            #################################################################
            # Лидер
            #################################################################
                act, arg = tmu.PROC_GOFWD, 0
                angle = 30
                if ((GlobalTimer % 100) == 0): act, arg = tmu.PROC_GOLEFT, angle*random.randint(-1, 1)
                # Рефлекс
                #if refl: act, arg = ract, rarg
                pass

            #################################################################
            a.Make(act, arg)

        GlobalTimer += 1
        if ((GlobalTimer % 100) == 0): print GlobalTimer

        r.sleep()

    gdic.terminate_program()

################################################################################
#
################################################################################
if __name__ == '__main__':

    if (len(sys.argv) < 4):
        print "\n", Title, "\n\nUsage is:", sys.argv[0], "envfile mapfile agentfile"
        sys.exit(1)

    envfile = sys.argv[1]
    mapfile = sys.argv[2]
    agentfile = sys.argv[3]

    main(envfile, mapfile, agentfile)
