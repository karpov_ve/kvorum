#
# flock moving demo
# Описание робота
#
random.seed(0)

TIP_LEADER = "hare"
TIP_WORKER = "turtle"

########################################################################
# Создание робота
########################################################################

def CreateRobot(id, pos, tip, irvalue):
    global TIP_LEADER, TIP_WORKER

    # Аргументы: cid, cpos, cshape, cenv, csize = 0
    A = TAgent(id, pos, tip, Env, csize = 0.5) # 0.3 0.65 0.75

    ##################################################################
    #
    # Сенсоры
    #
    # Аргументы: caddr, cid,  cdir, cang, cR, cLevel, ctip, cvaltype = gdic.ST_SCALAR, cfproc = None, cvalue = 0
    #
    ##################################################################

    #
    # *** Датчики препятствий
    #
    # USONIC левый передний
    USONIC_DIST = 20
    A.Sensors.append(TSensor(gdic.ADDR_MvCtl, 0, 0, 3, USONIC_DIST, gdic.LEVEL_GROUND, gdic.ST_USONIC))

    # USONIC правый передний
    A.Sensors.append(TSensor(gdic.ADDR_MvCtl, 1, 0, 3, USONIC_DIST, gdic.LEVEL_GROUND, gdic.ST_USONIC))


    # Датчики TSOP
    TSOP_DIST = 40
    A.Sensors.append(TSensor(gdic.ADDR_RC5Server, 0,   0, 90, TSOP_DIST, gdic.LEVEL_IR,  gdic.ST_DETECTOR, gdic.RST_SCALAR))
    A.Sensors.append(TSensor(gdic.ADDR_RC5Server, 1, 180, 90, TSOP_DIST, gdic.LEVEL_IR,  gdic.ST_DETECTOR, gdic.RST_SCALAR))
    A.Sensors.append(TSensor(gdic.ADDR_RC5Server, 2,  90, 90, TSOP_DIST, gdic.LEVEL_IR,  gdic.ST_DETECTOR, gdic.RST_SCALAR))
    A.Sensors.append(TSensor(gdic.ADDR_RC5Server, 3, -90, 90, TSOP_DIST, gdic.LEVEL_IR,  gdic.ST_DETECTOR, gdic.RST_SCALAR))

    # Агент становится источником сигнала
    # Это нужно для определения типа агента
    A.SetSrc(gdic.LEVEL_IR, irvalue)

    # Крейсерские скорости движения и разворота
    if(tip==TIP_WORKER):
        A.RobotMoveSpeed = 3
    else:
        A.RobotMoveSpeed = 1
    A.RobotRotateSpeed = 5

    return A

########################################################################
# Роботы
########################################################################
NUM_ROBOTS = 5
WORKER_IR_CODE = 0x01
LEADER_IR_CODE = WORKER_IR_CODE+100 #0xFF

#
# Агенты являются источниками ИК-излучения, код которого не постоянный, а зависит от id агента
#

x0 = Env.DIM_X/2
y0 = Env.DIM_Y/2
id = 1

for i in range(0, NUM_ROBOTS):
    Agents.append(CreateRobot(id, [x0+i*2, y0, 90], TIP_WORKER, WORKER_IR_CODE+id))
    id+=1
    Agents.append(CreateRobot(id, [x0+i*2, y0+2, 90], TIP_WORKER, WORKER_IR_CODE+id))
    id+=1
    Agents.append(CreateRobot(id, [x0+i*2, y0+4, 90], TIP_WORKER, WORKER_IR_CODE+id))
    id+=1

# Лидер
Agents.append(CreateRobot(255, [x0, y0+20, 90], TIP_LEADER, LEADER_IR_CODE))
