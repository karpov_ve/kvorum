#!/usr/bin/env python
# coding: utf-8
"""
  flock hunting

  Стайная охота с определением локального лидера.
  Роботы оснащены суперлокаторами.

  06.02.15
  Version 1.04
  LP 17.05.2015
"""
import os, sys, time
import roslib, rospy, time, random

LIB_PATH = "src/kvorum/pylib"

sys.path.append("../../pylib")
sys.path.append(LIB_PATH)

import gdic, rcproto, tmurobot as tmu
from env import TEnv
from agent import TAgent, TSensor

###################################################################

Title = "Demo 1.04"

GlobalTimer = 0
Env = None
Agents = []

# Инициалиизация системы
def InitSystem(nodename, envfilename, mapfilename, agentfilename):
    global Agents

    # Инициалиизация ROS
    rospy.init_node(nodename)

    # Входной топик
    # Подписываемся на топик ardans, второй параметр - объем кэша отправки
    inpqlen = len(Agents)*7
    rospy.Subscriber("/ardans_topic", tmu.ans, tmu.rsans_callback, queue_size=inpqlen)

    # Выходной топик
    outqlen = len(Agents)*7
    pub_cmd = rospy.Publisher("/actions_topic", tmu.action, queue_size=outqlen)

    # Создаем роботов
    for a in Agents:
        r = tmu.TRobot(a,pub_cmd)
        tmu.Robots.append(r)

########################################################################
#
########################################################################

def findat(R, a1, a2, code, delta):
    num = 0
    maxcode = 0
    L = len(R.agent.SuperLocator)
    if(a1>=L or a2>=L): return 0, 0
    for n in range(a1, a2):
        dist, val = R.agent.SuperLocator[n]
        if(val != 0):
            if((code == -1) or (abs(code-val) <= delta)): 
                num+=1
                if(val>maxcode): maxcode = val
    return num, maxcode
#
# q - номер квадранта:
#    0 - впереди
#    1 - сзади
#    2 - слева
#    1 - справа
#  углы a1, a2 - в диапазоне 0..360
#  [0]   180
#  [45]  135
#  [90]:  90
# [135]:  45
# [225]: -45
# [315]:-135
# [359]:-180
def FindIR(a, q, code, delta):
    val = 0
    if(q==0):
        a1, a2 = 135, 225
    if(q==1):
        a1, a2 = 0, 45
        a3, a4 = 315, 359
    if(q==2):
        a1, a2 = 45, 135
    if(q==3):
        a1, a2 = 225, 315
    val, maxcode = findat(a, a1, a2, code, delta)
    if(val!=0): return val, maxcode
    if(q==1):
        val, maxcode = findat(a, a3, a4, code, delta)
    return val, maxcode

def Analyze(A, friendcode, enemycode):
    delta = 20

    sit = {'fwd':[0,0], 'back':[0,0], 'left':[0,0], 'right':[0,0]}  

    sit['fwd'][0], f1 = FindIR(A, 0, friendcode, delta)
    sit['back'][0], f2 = FindIR(A, 1, friendcode, delta)
    sit['left'][0], f3 = FindIR(A, 2, friendcode, delta)
    sit['right'][0], f4 = FindIR(A, 3, friendcode, delta)

    sit['fwd'][1], e1 = FindIR(A, 0, enemycode, delta)
    sit['back'][1], e2 = FindIR(A, 1, enemycode, delta)
    sit['left'][1], e3 = FindIR(A, 2, enemycode, delta)
    sit['right'][1], e4 = FindIR(A, 3, enemycode, delta)

    nfriend = sit['fwd'][0]+sit['back'][0]+sit['left'][0]+sit['right'][0]
    nenemy  = sit['fwd'][1]+sit['back'][1]+sit['left'][1]+sit['right'][1]
    maxfriend = max(f1, f2, f3, f4)

    return sit, nfriend, nenemy, maxfriend

########################################################################
# Вспомогательные функции для обращения к датчикам
########################################################################

def sGetFL(A):
    s = A.agent.MainSensors[0]
    if(s==0): s = 100
    return s

def sGetFR(A):
    s = A.agent.MainSensors[1]
    if(s==0): s = 100
    return s

def GetLightSensor(A): return A.Dataserver[0]

def ShowStatus(A, sit):
    print A.agent.id, sit, GetLightSensor(A)

########################################################################
#
########################################################################

def MakeReflex(a): 
    rdist = 5
    act, arg = tmu.PROC_GOFWD, 0
    refl = True
    if(sGetFL(a)<rdist and sGetFR(a)<rdist):
        act, arg = tmu.PROC_GOLEFT, 5 + random.randint(-1, 1)
    elif(sGetFL(a)<rdist):
        act, arg = tmu.PROC_GORIGHT,5 + random.randint(-1, 1)
    elif(sGetFR(a)<rdist):
        act, arg = tmu.PROC_GOLEFT, 5 + random.randint(-1, 1)
    else:
        refl = False
    return refl, act, arg  

################################################################################
# MAIN
################################################################################

def main(envfilename, mapfilename, agentfilename):

    global Env, Agents, GlobalTimer

    # Инициалиизация системы
    print "Init environment... "
    Env = TEnv(envfilename)

    print "Init map", mapfilename, "..."
    exec (open(mapfilename).read())

    print "Init agents", agentfilename, "..."
    exec (open(agentfilename).read())

    InitSystem('demo', envfilename, mapfilename, agentfilename)

    print "Start main loop"
    r = rospy.Rate(10) # 50hz

    #####################################################
    # Основной цикл
    #####################################################

    while not rospy.is_shutdown():
        for a in tmu.Robots:
            if not a.agent.alive: continue
            a.RequestAllSensors(False)

            act, arg = tmu.PROC_GOFWD, 0

            #################################################################
            # Хищник
            #################################################################
            if(a.agent.shape==TIP_HUNTER):
                refl, ract, rarg = MakeReflex(a)
                sit, nfriend, nenemy, maxfriend = Analyze(a, HUNTER_IR_CODE, VICTIM_IR_CODE)
                isLeader = (a.agent.GetSrc(gdic.LEVEL_IR)>maxfriend)
                print a.agent.id, ":", maxfriend, isLeader
                #ShowStatus(a, sit)
                angle = 10
                if(nenemy>0): # Ловим
                    if sit['fwd'][1]>0: # Жертва впереди
                        act, arg = tmu.PROC_GOFWD, 0
                    elif sit['left'][1]>0: # Жертва слева
                        act, arg = tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
                    elif sit['right'][1]>0: # Жертва справа
                        act, arg = tmu.PROC_GORIGHT, angle + random.randint(-1, 1)
                    elif sit['back'][1]>0: # Жертва сзади
                        act, arg = tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
                elif(nfriend>0): # Сближаемся со своими
                    # Агент слабее. Сближаемся
                    if(not isLeader):
                        if sit['fwd'][0]>0: act, arg = tmu.PROC_GOFWD, 0
                        elif sit['left'][0]>0:  act, arg = tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
                        elif sit['right'][0]>0: act, arg = tmu.PROC_GORIGHT, angle + random.randint(-1, 1)
                        elif sit['back'][0]>0:  act, arg = tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
                        a.agent.SetState(gdic.STAT_NORM)
                    else:
                    # Агент сильнее. Лидер не подчиняется правилам сближения
                        a.agent.SetState(gdic.STAT_LEADER)
                        act, arg = tmu.PROC_GOFWD, 0
                else: # Ничего нет
                    act, arg = tmu.PROC_GOFWD, 0
                # Пробуем съесть
                nv = GetLightSensor(a)
                if(nv!=0):
                    act, arg = tmu.PROC_KILL, nv

            #################################################################
            # Жертва
            #################################################################
            else:
                refl, ract, rarg = MakeReflex(a)
                sit, nfriend, nenemy, maxfriend = Analyze(a, VICTIM_IR_CODE, HUNTER_IR_CODE)
                #ShowStatus(a, sit)
                angle = 10
                if(nenemy>0): # Убегаем
                    if sit['fwd'][1]>0: # Опасность впереди
                        act, arg = tmu.PROC_GOBACK, 0 #tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
                    elif sit['left'][1]>0: # Опасность слева
                        act, arg = tmu.PROC_GORIGHT, angle + random.randint(-1, 1)
                    elif sit['right'][1]>0: # Опасность справа
                        act, arg = tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
                    elif sit['back'][1]>0: # Опасность сзади
                        act, arg = tmu.PROC_GOFWD, 0
                elif(nfriend>0): # Сближаемся со своими
                    if sit['fwd'][0]>0: act, arg = tmu.PROC_GOFWD, 0
                    elif sit['left'][0]>0:  act, arg = tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
                    elif sit['right'][0]>0: act, arg = tmu.PROC_GORIGHT, angle + random.randint(-1, 1)
                    elif sit['back'][0]>0:  act, arg = tmu.PROC_GOLEFT, angle + random.randint(-1, 1)
                else: # Ничего нет
                    act, arg = tmu.PROC_GOFWD, 0

            #################################################################
            # Рефлекс
            if refl: act, arg = ract, rarg
            a.Make(act, arg)

        GlobalTimer += 1

        r.sleep()

    gdic.terminate_program()

################################################################################
#
################################################################################
if __name__ == '__main__':

    if (len(sys.argv) < 4):
        print "\n", Title, "\n\nUsage is:", sys.argv[0], "envfile mapfile agentfile"
        sys.exit(1)

    envfile = sys.argv[1]
    mapfile = sys.argv[2]
    agentfile = sys.argv[3]

    main(envfile, mapfile, agentfile)
