# gbp
# Параметры поля
# Этот файл читается при создании объекта Env
#

# Ширина и высота окна отображения поля
self.MAX_SCR_X = 500
self.MAX_SCR_Y = 500

# Размерность поля
self.DIM_X = 200
self.DIM_Y = 200

self.UseTorusRegime  = False  # Флаг режима топологии тора
