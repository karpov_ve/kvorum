#!/usr/bin/env python3
# coding: utf-8
"""
  main.py
  KVORUM MAS - MODEL
  Author: Valery Karpov
  06.02.15
  Version 1.09
  LP 12.06.2021

"""
from __future__ import print_function

import os, sys, time, random, math
import roslib, rospy

import gdic, rcproto

from env import TEnv
from agent import TAgent, TSensor, TEncoder

# Топики ROS
from msg_rsaction.msg import action
from msg_ans.msg import ans
from msg_kvorum.msg import viz as viz_proc
from msg_kvorum.msg import pos as viz_pos

###################################################################
Title = "Kvorum-M 1.09"

GlobalTimer = 0
Env = None
Agents = []

###################################################################

# Отправка пакета
def SendPackage(P):
    global pub_cmd
    global GlobalTimer

    pkglen = P.getpkglen();

    if(pkglen>=rcproto.RC_MAX_BUFF):
        print("\n*** ", pkglen, rcproto.RC_MAX_BUFF)
        gdic.error("SendPackage: package len error ")

    # Пересылаем пакет в исходном виде
    msgans = ans()
    msgans.id = rcproto.MY_ADDR
    msgans.result = P.getcmd()
    msgans.data = P.rcBUFF
    msgans.tm = GlobalTimer # rospy.get_rostime() # time(NULL)
    pub_cmd.publish(msgans)
    return

def Afind(id):
    for a in Agents:
        if(a.id == id): return a
    print("AFind id=", id)
    gdic.error("agent not found")

def rsaction_callback(cmdmsg):
    cmd = cmdmsg.action
    aid = cmdmsg.agent_id
    arg1 = cmdmsg.arg1
    arg2 = cmdmsg.arg2
    arg3 = cmdmsg.arg3

    if cmd != rcproto.CMD_F_SET_FIELD:
        rcproto.MY_ADDR = aid
    else:
        rcproto.MY_ADDR = 0

    # sic
    print('{} {} cmd: {:02X}h args: {} {} {}'.format(cmdmsg.team_id, aid, cmd, arg1, arg2, arg3))

    uncknowncommand = False
    SERVER_ADDR = 100
    p = rcproto.TPckg()
    # Формируем пакет
    p.rcFormPackageCMDNoArg(SERVER_ADDR, rcproto.CMD_ACK)

    if cmd != rcproto.CMD_F_SET_FIELD:
        A = Afind(aid)
    #
    # Данные
    #
    # Основные сенсоры
    #
    if(cmd == rcproto.CMD_GET_SENS):
        sensors = A.GetMainSensors()
        p.Data2Pckg(sensors, SERVER_ADDR, rcproto.CMD_ANS_GET_SENS)
    #
    # Локатор
    #
    elif(cmd == rcproto.CMD_GET_USR_DATA):
        locator = A.GetLocator()
        p.Data2Pckg(locator, SERVER_ADDR, rcproto.CMD_ANS_GET_USR_DATA)
    #
    # Суперлокатор
    #
    elif(cmd == rcproto.CMD_F_GET_SUPER_LOCATOR):
        locator = A.GetSuperLocator()
        p.SuperData2Pckg(locator, SERVER_ADDR, rcproto.CMD_F_ANS_GET_SUPER_LOCATOR)
    #
    # Все регистры
    #
    elif(cmd == rcproto.CMD_GET_ALL_REG):
        registers = A.GetAllRegisters()
        p.Data2Pckg(registers, SERVER_ADDR, rcproto.CMD_ANS_GET_ALL_REG)
    elif(cmd == rcproto.CMD_GET_REG):
        regval = A.GetRegister(arg1)
        p.rcFormPackage1B(SERVER_ADDR, rcproto.CMD_ANS_REG, regval)
    #
    #  I2C
    #
    elif(cmd == rcproto.CMD_GET_I2C_DATA):
        s = A.ReadSensors(arg1)
        # Добавляем адрес в начало
        sensors = [];
        sensors.append(arg1)
        for i in range(0, len(s)):
            sensors.append(s[i])
        p.Data2Pckg(sensors, SERVER_ADDR, rcproto.CMD_ANS_GET_I2C_DATA)
    #
    # Движение
    #
    elif(cmd == rcproto.CMD_FWD):
        A.Set2Speed(A.RobotMoveSpeed, 0)
    elif(cmd == rcproto.CMD_BACK):
        A.Set2Speed(-A.RobotMoveSpeed, 0)
    elif(cmd == rcproto.CMD_SET_SPEED):
        A.SetSidesSpeed(arg1, arg2)
    elif(cmd == rcproto.CMD_LEFT or cmd == rcproto.CMD_FAST_LEFT):
        if(arg1==0): # Постоянный разворот
            A.Set2Speed(0, A.RobotRotateSpeed)
        else: # Поворот на угол
            A.Set2Speed(0, 0)
            A.Turn2(arg1)
    elif(cmd == rcproto.CMD_RIGHT or cmd == rcproto.CMD_FAST_RIGHT):
        if(arg1==0): # Постоянный разворот
            A.Set2Speed(0, -A.RobotRotateSpeed)
        else: # Поворот на угол
            A.Set2Speed(0, 0)
            A.Turn2(-arg1)
    elif(cmd == rcproto.CMD_STOP):
        A.Set2Speed(0, 0)
    elif(cmd == rcproto.CMD_DEBUG): # Пробуем убрать поле вокруг агента a на слое arg1
        # sic
        # arg1 = gdic.LEVEL_COLOR
        EraseField(A, arg1)
    elif(cmd == rcproto.CMD_F_DELETE_AGENT): # Удалить агента. Аргумент: id агента
        A.Delete()
    elif(cmd == rcproto.CMD_F_SET_APOS): # Установить координаты агента. Аргументы: id агента, x, y, angle
        newpos = (arg1, arg2, arg3)
        A.MoveTo(newpos)

    elif(cmd == rcproto.CMD_F_SET_A_SRC): # Агент становится источником сигнала value на уровне level
        A.SetSrc(arg1, arg2)              # level, val

    elif(cmd == rcproto.CMD_F_SET_FIELD): # Установить значение поля. Аргументы: arg1=x, arg2=y, arg3=level, id=value
        SetField(arg1, arg2, arg3, aid)
    elif(cmd == rcproto.CMD_F_SET_STATE): # Установить статус агента. Аргумент: arg1=mode
        SetState(aid, arg1)

    #
    # RC5 Server
    #
    # Команда I2C-устройству. Формат: <i2c-адрес устройства> <команда> <количество аргументов> <аргумент1> <аргумент2> ...
    #
    #elif(cmd == rcproto.CMD_I2C):
    #    i2caddr = arg1
    #    i2ccmd = arg2
    """
      // RC5 Server
      case 'Z': // Start gen
                cmd = erl::ERCMD_I2C;
                arg1 = RC5Server::ADDR;
                arg2 = RC5Server::DCMD_GENERATE;
                datalen = RC5Server::FormRawDataBuff(d0, buff);
                for(int i=0;i<datalen;i++)
                  cmdmsg.data.push_back(buff[i]);
                break;
      case 'X': // Stop gen
                cmd = erl::ERCMD_I2C;
                arg1 = RC5Server::ADDR;
                arg2 = RC5Server::DCMD_STOP_GENERATE;
                break;
      case 'C': // Clear data
                cmd = erl::ERCMD_I2C;
                arg1 = RC5Server::ADDR;
                arg2 = RC5Server::DCMD_CLEAR_DATA;
                break;
      case 'V': // Read data
                cmd = erl::ERCMD_GET_I2C;
                arg1 = RC5Server::ADDR;
                arg2 = RC5Server::DATALEN;
                break;
    """

    """
    if(cmd == CMD_BEEP):
    elif(cmd == CMD_GET_USR_DATA):
    elif(cmd == CMD_BEEP_ON):
    elif(cmd == CMD_BEEP_OFF):
      rcSendCMDNoArg(GetPckg(ADDR), ADDR, cmd);
    elif(cmd == CMD_FWD2):
    elif(cmd == CMD_BACK2):
    elif(cmd == CMD_FAST_LEFT2):
    elif(cmd == CMD_FAST_RIGHT2):
      rcSendCMD1Arg(GetPckg(ADDR), ADDR, cmd, arg1);
    elif(cmd == CMD_SET_REG):
      rcSendCMD2Arg(GetPckg(ADDR), ADDR, cmd, arg1, arg2);
      if(arg1==REG_ACK)
        AckRegime = arg2; // Режим подтверждения
    # Команда I2C-устройству. Формат: <i2c-адрес устройства> <команда> <количество аргументов> <аргумент1> <аргумент2> ...
    elif(cmd == CMD_I2C):
      i2caddr = arg1;
      i2ccmd = arg2;
      i2clen = msgData2Buff(msg, i2cbuff);
      rcSendI2CDataArray(GetPckg(ADDR), ADDR, cmd, i2caddr, i2ccmd, i2clen, i2cbuff);
    else:
      uncknowncommand = True
    """
    # Отправляем подтверждение
    if(not uncknowncommand):
        SendPackage(p)

################################################################################
# MAIN
################################################################################

# Отправка команды инициализации всех агентов
def InitAllAgents():
    global pub_viz
    print("Init all agents... ")
    # Инициализируем агентов для рисования
    msg = viz_proc()
    msg.cmd = gdic.VIZ_CMD_AINIT
    for a in Agents:
        d = viz_pos()
        d.id = a.id
        d.x, d.y, d.a = int(a.pos[0]), int(a.pos[1]), int(a.pos[2])
        d.cs = a.size
        d.shape = a.shape
        d.traceOn = a.traceOn
        d.show_id = a.show_id
        msg.data.append(d)
    pub_viz.publish(msg)
    print("Done")

# Отправка команды рисования всех агентов
def SendDrawAgentsCommand():
    global pub_viz
    msg = viz_proc()
    msg.cmd = gdic.VIZ_CMD_ADRAW
    for a in Agents:
        d = viz_pos()
        d.id =  a.id
        d.x, d.y, d.a = int(a.pos[0]), int(a.pos[1]), int(a.pos[2])
        d.traceOn = a.traceOn
        msg.data.append(d)
    pub_viz.publish(msg)

def EraseField(a, level):
    global pub_viz
    msg = viz_proc()
    msg.cmd = gdic.VIZ_CMD_SET_FIELD
    x = int(a.pos[0])
    y = int(a.pos[1])
    radius = 2
    for i in range (x-radius, x+radius+1):
        for j in range (y-radius, y+radius+1):
            ix, iy = Env.normalizate_xy(i, j)
            Env.SetFieldVal(ix, iy, level, 0)
            print(ix, iy, level, "->", 0)
            d = viz_pos()
            d.x = ix
            d.y = iy
            d.a = level
            d.id = 0
            msg.data.append(d)
    pub_viz.publish(msg)

# Установить значение поля
def SetField(x, y, level, value):
    global pub_viz
    Env.SetFieldVal(x, y, level, value)
    msg = viz_proc()
    msg.cmd = gdic.VIZ_CMD_SET_FIELD
    d = viz_pos()
    d.x = x
    d.y = y
    d.a = level
    d.id = value
    msg.data.append(d)
    pub_viz.publish(msg)

# Установить статус агента
def SetState(aid, mode):
    global pub_viz
    msg = viz_proc()
    msg.cmd = gdic.VIZ_CMD_SET_STATE
    d = viz_pos()
    d.id = aid
    d.state = mode
    msg.data.append(d)
    pub_viz.publish(msg)

#
#
#
def main(envfilename, mapfilename, agentfilename):
    global Env, Agents, GlobalTimer
    global pub_cmd, pub_viz

    ############################################################################
    # Инициалиизация системы моделирования
    ############################################################################

    print("Init environment... ")
    Env = TEnv(envfilename)

    print("Init map", mapfilename, "...")
    exec (open(mapfilename).read())

    print("Init agents", agentfilename, "...")
    exec (open(agentfilename).read())

    ############################################################################
    # Инициалиизация ROS
    ############################################################################
    rospy.init_node('kvorum_m')

    # Входной топик
    inpqlen = len(Agents)*7
    sub_act = rospy.Subscriber("/actions_topic", action, rsaction_callback,
            queue_size=max(1,inpqlen))

    # Выходные топики
    outqlen = len(Agents)*7
    pub_cmd = rospy.Publisher("/ardans_topic", ans, queue_size=outqlen)
    pub_viz = rospy.Publisher("/viz_proc_topic", viz_proc, queue_size=outqlen)

    #####################################################
    # Инициализируем агентов для рисования
    #####################################################
    time.sleep(1)

    # Отправка команды инициализации всех агентов
    InitAllAgents()
    time.sleep(1)

    #####################################################
    # Основной цикл
    #####################################################
    print("Start main loop")

    r = rospy.Rate(Env.Rate) # 50hz

    GlobalTimer = 0
    dratio = 0

    while not rospy.is_shutdown():
        for a in Agents:
            a.Step()

        GlobalTimer += 1

        #
        # Рисуем
        #
        dratio+=1
        if(dratio>Env.Rate/5): # Env.Rate 10 20
            dratio = 0
            SendDrawAgentsCommand()

        r.sleep()

    gdic.terminate_program()

################################################################################
#
################################################################################
if __name__ == '__main__':

    if (len(sys.argv) < 4):
        print("\n"+Title, "\n\nUsage is:", sys.argv[0], "envfile mapfile agentfile")
        sys.exit(1)

    envfile = sys.argv[1]
    mapfile = sys.argv[2]
    agentfile = sys.argv[3]

    main(envfile, mapfile, agentfile)
